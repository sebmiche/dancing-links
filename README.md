This is an unfinished proof of an OCaml implementation of Knuth's algorithm
DLX.

## Dependencies

This proof uses Arthur Charguéraud's TLC and CFML libraries. With a new switch:

    % opam switch create dlx 4.05.0
    % opam install -j 2 coq.8.8.0
    % opam repo add coq-released https://coq.inria.fr/opam/released
    % opam install -j 2 coq-cfml
    % opam install -j 2 coqide.8.8.0

## Building the sources

Just `make` in the `src` directory to build the Dancing Links module and an
exemple program, `queens`, that solves the n-queens problem.

A C implementation is available in `src-c` for performance comparisons.

## Building the proof

From the raw clone, first make the `cfml` directory to build the characteristic
formulae of `cfml/src/DancingLinks.ml`, a minimal version of the
implementation from `src`.

Then build the Coq code by making the `proof` directory:

    % make -C cfml
    % make -C proof

The only unfinished file is `DancingLinks.v`; it builds but lacks some proofs
and theorems.

## Testing in CoqIDE

You can use the `coqide.sh` script to start CoqIDE with the appropriate library
paths; just edit the `$switch` variable in the script and `source` it in a
terminal.
