(*
**  Permutation.v: CFML proof of the Permutation module.
*)

Set Implicit Arguments.

Require Import CFML.CFLib.
Require Import CFML.Stdlib.Pervasives_proof.
Require Import TLC.LibListZ.
Require Import TLC.LibFun.
Require Import Stdlib.
Require Import Array_proof.

Require Import Extensions.
Require Import FunctionalExtensionality.
Require Import PROOFS.DancingLinks_ml.
Require Import LibPermutation.

Implicit Types n: int.
Implicit Types D: set int.
Implicit Types ϕ ψ: int -> int.
Implicit Types next prev: list int.

(* Permutation invariants.
   @n is the number of elements in the permutation (elements are 0..n-1)
   @D is the set of currently-visible elements (sub-domain of the permutation)
   @ϕ is the permutation function, it must be a bijection over D
   @ψ is the inverse function, it must invert ϕ on D
   Note that we don't specify what ϕ and ψ are like outside D. *)
Record inv n D ϕ ψ next prev := {
  IP    : Permutation D ϕ ψ;
  Idom  : forall k, k ∈ D -> index n k;
  Iszn  : index next n;
  Iszp  : index prev n;
  Iϕ    : forall k, index n k -> next[k] = ϕ k;
  Iψ    : forall k, index n k -> prev[k] = ψ k;
}.

Arguments Idom [n] [D] [ϕ] [ψ] [next] [prev] i [k].

(* Representation predicate of a Permutation.perm object *)
Definition Perm n D ϕ ψ (p: loc) :=
  Hexists (next: list int) (prev: list int) pnext pprev,
    p ~> `{
      next' := pnext;
      prev' := pprev
    } \*
    pnext ~> Array next \*
    pprev ~> Array prev \*
    \[ inv n D ϕ ψ next prev ].

(** Folding and unfolding lemmas for the record **)

Lemma Perm_open : forall p n D ϕ ψ,
  p ~> Perm n D ϕ ψ ==>
  Hexists (next: list int) (prev: list int) pnext pprev,
    p     ~> `{ next' := pnext; prev' := pprev } \*
    pnext ~> Array next \*
    pprev ~> Array prev \*
    \[ inv n D ϕ ψ next prev ].
Proof. intros. xunfolds~ Perm. Qed.

Lemma Perm_close : forall p n D ϕ ψ next prev pnext pprev,
  p     ~> `{ next' := pnext; prev' := pprev } \*
  pnext ~> Array next \*
  pprev ~> Array prev \*
  \[ inv n D ϕ ψ next prev ] ==>
  p ~> Perm n D ϕ ψ.
Proof. intros. xunfolds~ Perm. Qed.

Hint Extern 1 (RegisterOpen (Perm _ _ _ _))   => Provide Perm_open.
Hint Extern 1 (RegisterClose (record_repr _)) => Provide Perm_close.

(** Extracting invariants from a folded Perm **)

Lemma perm_inv_index : forall p n D ϕ ψ k,
  k ∈ D -> p ~> Perm n D ϕ ψ ==+> \[ index n k ].
Proof.
  intros. xopen p. xpull ;=> next prev pnext pprev I.
  xclose p. apply I. hsimpl. destruct I. auto.
Qed.

Lemma perm_inv_permutation: forall p n D ϕ ψ,
  p ~> Perm n D ϕ ψ ==+> \[ Permutation D ϕ ψ ].
Proof. intros. xopen p. xpull. introv H. inversion H. xclose~ p. eauto. Qed.

(** Helpers: function update and proof automation **)

(* rew_update: unfold fupdate, bruteforce, unfold list update, bruteforce *)
Ltac rew_update :=
  simpl; unfold fupdate; repeat case_if~;
  try rewrite read_update_case; try case_if~.
(* rew_update_at : start with some inner update to reduce branching *)
Ltac rew_update_at n :=
  simpl; unfold fupdate at n; repeat case_if~; rew_update.

Tactic Notation "rew_update" "at" integer(n) := rew_update_at n.

(* Index automation *)

(* inv_index: extract index invariants *)
Ltac inv_index :=
  repeat match goal with
  | I : inv _ _ _ _ _ _ |- _ =>
    lets : (Iszn I); lets : (Iszp I); lets : (Idom I); clear dependent I
  end.

Hint Extern 1 (index _ _) =>
  inv_index;
  try rewrite index_update_eq in *;
  rew_index.

(* Representation of functions by arrays *)

(* inv_repr: extract representation invariants *)
Ltac inv_repr :=
  repeat match goal with
  | I : inv _ _ _ _ _ _ |- _ =>
    lets : (Iϕ I); lets : (Iψ I); clear dependent I
  end.

Hint Extern 2 (_ [ ?k ] = _ ?k) => inv_repr.
Hint Extern 2 (?f _ = ?f _) => fequal.

(** Basic functions : next and prev **)

Lemma next_spec : forall p i n D ϕ ψ,
  index n i ->
  app next [p i]
    PRE  p ~> Perm n D ϕ ψ
    POST \[= ϕ i] \*+ p ~> Perm n D ϕ ψ.
Proof. introv Hrange. xcf. xopen p. xpull ;=>. xapps. xapp~. xclose* p. Qed.

Lemma prev_spec : forall p i n D ϕ ψ,
  index n i ->
  app prev [p i]
    PRE  p ~> Perm n D ϕ ψ
    POST \[= ψ i] \*+ p ~> Perm n D ϕ ψ.
Proof. introv Hrange. xcf. xopen p. xpull ;=>. xapps. xapp~. xclose* p. Qed.

Hint Extern 1 (RegisterSpec next) => Provide next_spec.
Hint Extern 1 (RegisterSpec prev) => Provide prev_spec.

(** Hiding and showing elements (Knuth's trick) **)

Lemma hide_spec : forall p y n D ϕ ψ,
  y ∈ D ->
  app hide [p y]
    PRE  p ~> Perm n D ϕ ψ
    POST # p ~> Perm n (D \-- y) ϕ[ψ y := ϕ y] ψ[ϕ y := ψ y]
         \* \[ ϕ[ψ y := ϕ y] y ∈ D /\ ψ[ϕ y := ψ y] y ∈ D ].
Proof.
  introv Dy. xcf. xchange (>> perm_inv_index Dy). xpull ;=>.
  xapps~. xapps~.
  xopen p. xpull ;=> next prev pnext pprev I. lets P : (IP I).
  xapps. xapp. lets~ : (Idom I (Pimgψ P Dy)).
  xapps. xapp. lets~ : (Idom I (Pimgϕ P Dy)).
  xclose (>> p n (D \-- y) (ϕ[ψ y := ϕ y]) (ψ[ϕ y := ψ y])). constructor.

  (* Prove that the result is still a permutation *)
  { constructor.
    (* ψ' is the inverse of ϕ'. *)
    { introv Dk. tests~ : (k = y). weakset in Dk. rew_update.
      apply (Pinjϕ P Dk Dy) in C0. false_invert. apply (Pinvϕ P Dk). }
    (* ϕ' is the inverse of ψ'. *)
    { introv Dk. tests~ : (k = y). weakset in Dk. rew_update.
      apply (Pinjψ P Dk Dy) in C0. false_invert. apply (Pinvψ P Dk). }
    (* ϕ' preserves domain *)
    { introv Dk. tests~ : (k = y). rew_update.
      tests : (ϕ y = y). apply (f_equal ψ) in C1.
      rewrite (Pinvϕ P Dy) in C1. false_invert. lets~ : (Pimgϕ P Dy).
      weakset in Dk. tests : (ϕ k = y). rewrites~ (Pinvϕ P Dk) in C0.
      lets~ : (Pimgϕ P Dk). }
    (* ψ' preserves domain *)
    { introv Dk. tests~ : (k = y). rew_update.
      tests : (ψ y = y). apply (f_equal ϕ) in C1.
      rewrite (Pinvψ P Dy) in C1. false_invert. lets~ : (Pimgψ P Dy).
      weakset in Dk. tests : (ψ k = y). rewrites~ (Pinvψ P Dk) in C0.
      lets~ : (Pimgψ P Dk). } }

  (* Prove that D' does not overflow the arrays *)
  { introv Dk. weakset in Dk. lets~ : (Idom I Dk). }
  (* Prove that the new arrays are still large enough *)
  auto. auto.
  (* Prove that the new arrays indeed encode ϕ' and ψ' *)
  { introv Hk. rew_update. auto. eauto. auto. }
  { introv Hk. rew_update. auto. eauto. auto. }

  (* Final pieces of post-condition *)
  hsimpl. split.
  lets~ : (Pimgϕ P Dy). rew_update.
  lets~ : (Pimgψ P Dy). rew_update.
Qed.

Lemma show_spec : forall p y n D ϕ ψ,
  index n y -> y ∉ D ->
  ϕ y ∈ (D \++ y) -> ψ y ∈ (D \++ y) ->
  ϕ (ψ y) = ϕ y -> ψ (ϕ y) = ψ y ->
  app show [p y]
    PRE  p ~> Perm n D ϕ ψ
    POST # p ~> Perm n (D \++ y) ϕ[ψ y := y] ψ[ϕ y := y].
Proof.
  introv Hy Dy Hϕy Hψy Hskipϕ Hskipψ. xcf. xapps~. xapps~.
  xopen p. xpull ;=> next prev pnext pprev I. lets P : IP I.
  xapps. xapp. tests~ : (ψ y = y). strongset in Hψy. lets~ : (Idom I Hψy).
  xapps. xapp. tests~ : (ϕ y = y). strongset in Hϕy. lets~ : (Idom I Hϕy).
  xclose (>> p n (D \++ y) (ϕ[ψ y := y]) (ψ[ϕ y := y])). constructor.

  (* Prove that the result is still a permutation *)
  { constructor.
    (* ψ' is the inverse of ϕ' *)
    { introv Dk. tests : (k = ψ y). rew_update at 2.
        rewrite <- C0 in Hskipϕ. tests~ : (ψ y = y).
        strongset in Hψy. lets~ : (Pimgϕ P Hψy).
      tests : (k = y). rew_update. strongset in Dk.
      rew_update at 2. apply (f_equal ψ) in C2. rewrite (Pinvϕ P Dk) in C2.
      rewrites~ Hskipψ in C2. lets~ : (Pinvϕ P Dk). }
    (* ϕ' is the inverse of ψ' *)
    { introv Dk. tests : (k = ϕ y). rew_update at 2.
        rewrite <- C0 in Hskipψ. tests~ : (ϕ y = y).
        strongset in Hϕy. lets~ : (Pimgψ P Hϕy).
      tests : (k = y). rew_update. strongset in Dk.
      rew_update at 2. apply (f_equal ϕ) in C2. rewrite (Pinvψ P Dk) in C2.
      rewrites~ Hskipϕ in C2. lets~ : (Pinvψ P Dk). }
    (* ϕ' preserves domain *)
    { introv Dk. rew_update. tests~ : (k = y).
      strongset in Dk. lets~ : (Pimgϕ P Dk). }
    (* ψ' preserves domain *)
    { introv Dk. rew_update. tests~ : (k = y).
      strongset in Dk. lets~ : (Pimgψ P Dk). } }

  (* Prove that D' does not overflow the arrays *)
  { introv Dk. tests~ : (k = y). strongset in Dk. lets~ : (Idom I). }
  (* Prove that the new arrays are still large enough *)
  auto. auto.
  (* Prove that the new arrays indeed encore ϕ' and ψ' *)
  { introv Hk. rew_update. auto. eauto. auto. }
  { introv Hk. rew_update. auto. eauto. auto. }

  hsimpl.
Qed.

Hint Extern 1 (RegisterSpec hide) => Provide hide_spec.
Hint Extern 1 (RegisterSpec show) => Provide show_spec.

(** If [f] preserves the permutation, then so does [hide; f; show] **)

Lemma revert_update : forall D ϕ ψ y,
  Permutation D ϕ ψ -> y ∈ D ->
  ϕ = ϕ[ψ y := ϕ y][ψ [ϕ y := ψ y] y := y] /\
  ψ = ψ[ϕ y := ψ y][ϕ [ψ y := ϕ y] y := y].
Proof.
  introv P Dy. split; extensionality k; rew_update.
  rewrites~ <- (Pinvψ P Dy). rewrites~ <- (Pinvψ P Dy).
  rewrites~ <- (Pinvϕ P Dy). rewrites~ <- (Pinvϕ P Dy).
Qed.
