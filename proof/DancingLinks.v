(*
**  DancingLinks.v: Unfinished CFML proof of my OCaml DancingLinks module.
*)

Set Implicit Arguments.

Require Import CFML.CFLib.
Require Import TLC.LibInt.
Require Import TLC.LibList.
Require Import TLC.LibListZ.
Require Import TLC.LibMap.
Require Import TLC.LibProd.
Require Import TLC.LibSet.

Require Import Array_proof.

Require Import Extensions.
Require Import LibPermutation.
Require Import Permutation.
Require Import ExactCover.
Require Import PROOFS.DancingLinks_ml.



(** TPerm: Alternate representation predicate for permutations **)

(* The cover problem instance contains arrays of permutations, and the higher-
   order representation predicate [Arrayof] onlys supports predicates with a
   single argument. So we can't use the usual [Perm] with 4 arguments; instead,
   we define [TPerm] that takes all 4 as a tuple.
   This is quite bothersome but there aren't many alternatives. *)

Definition perm: Type := int * set int * (int -> int) * (int -> int).

Definition perm_n '((n, _, _, _): perm) := n.
Definition perm_D '((_, D, _, _): perm) := D.
Definition perm_ϕ '((_, _, ϕ, _): perm) := ϕ.
Definition perm_ψ '((_, _, _, ψ): perm) := ψ.

Instance Inhab_perm : Inhab perm.
Proof. apply Inhab_of_val. lets: (\{}: set int). repeat constructor~. Qed.

Definition TPerm '((n, D, ϕ, ψ): perm) :=
  Perm n D ϕ ψ.

Lemma TPerm_unfold: forall p a,
  a ~> TPerm p = a ~> Perm (perm_n p) (perm_D p) (perm_ϕ p) (perm_ψ p).
Proof. destruct p as [[[n D] ϕ] ψ]. auto. Qed.



(** Arrayof: Higher-order array predicate **)

(* Arrayof: This predicate is defined in Arthur Charguéraud's "Higher-order
   representation predicates in separation logic". The subset of elements owned
   by the array is defined as the domain of M.
   @A is the logical model of the elements
   @R is the representation predicate of the elements (A -> loc -> hprop)
   @l is the list of array elements (must be pointers)
   @M is the mapping from pointers to logical models *)
Definition Arrayof A (R: htype A loc) (l: list loc) (M: map loc A) p :=
  \[ dom M ⊆ to_set l ] \*
  p ~> Array l \*
  (* [Group] debotes the ownership of the elements by the array using an
     iterated star *)
  Group R M.

Parameter arrayof_get_spec :
  curried 2%nat Array_ml.get /\
  forall A (H: Inhab A) (R: htype A loc) (l: list loc) (M: map loc A) t i,
  index l i ->
  app Array_ml.get [t i]
    PRE  t ~> Arrayof R l M
    POST \[= l[i]] \*+ t ~> Arrayof R l M.

Parameter arrayof_own_spec:
  curried 2%nat Array_ml.get /\
  forall A (H: Inhab A) (R: htype A loc) (l: list loc) (M: map loc A) t i,
  index l i ->
  app Array_ml.get [t i]
    PRE  t ~> Arrayof R l M
    POST \[= l[i]] \*+ l[i] ~> R M[l[i]] \*+ t ~> Arrayof R l (M \-- l[i]).



(** Make map **)

(* The [make_map] function creates a map from two lists of keys and values. The
   lemmas below show that keys[i] is indeed bound to values[i]. *)

Fixpoint make_map A B (arr: list A) (qs: list B) : map A B :=
  match (arr, qs) with
  | (nil, _) => \{}
  | (_, nil) => \{}
  | (a :: arr', q :: qs') => (make_map arr' qs')[a := q]
  end.

Lemma make_map_inv:
  forall A B (HA: Inhab A) (HB: Inhab B) (arr: list A) (qs: list B) i,
  noduplicates arr -> index arr i -> index qs i ->
  binds (make_map arr qs) arr[i] qs[i].
Proof.
  induction arr.
  * intros. rew_index.
  * introv ? Hi Hq. assert (~ (qs = nil)).
    intro. subst. inversion Hq. rew_list in *. rew_index.
    tests: (i = 0).
    - rewrite read_zero. simpl. case_eq qs. auto.
      intros. rewrite read_zero. apply binds_update_same.
    - simpl. case_eq qs. auto. intros.
      repeat (rewrites read_cons_case; case_if~).
      assert (arr[i - 1] <> a). inv noduplicates. intro. assert (mem a arr).
      subst. apply mem_of_index. inversion Hi as [? Hb]. rew_list in Hb.
      rew_index. auto. subst.
      rewrites~ <- (>> map_update_outside a b). apply IHarr. inv~ noduplicates.
      inversion Hi as [? Hb]. rew_list in Hb. rew_index.
      inversion Hq as [? Hb]. rew_list in Hb. rew_index.
Qed.

Lemma read_make_map:
  forall A B (HA: Inhab A) (HB: Inhab B) (arr: list A) (qs: list B) i,
  noduplicates arr -> index arr i -> index qs i ->
  (make_map arr qs)[arr[i]] = qs[i].
Proof. intros. apply read_of_binds. apply~ make_map_inv. Qed.

Hint Rewrite read_make_map: rew_map.



(** Cycle permutations **)

(* This is the formalisation of cycle permutations, defined by the list of
   elements they contain. All properties of permutations can be deduced from
   there, so I really should have started with this instead of Permutation. *)

(* Cycle length (only makes sense when [Cycle p l] is true) *)
Definition cl (p: perm) := card (perm_D p).

Definition Cycle (p: perm) (l: list int): Prop :=
  Permutation (perm_D p) (perm_ϕ p) (perm_ψ p) /\
  list_repr (perm_D p) l /\
  (forall i, index (cl p - 1) i -> (perm_ϕ p) l[i] = l[i + 1]) /\
  (perm_ϕ p) l[cl p - 1] = l[0].

Tactic Notation "inv_cycle" :=
  match goal with
  | h: Cycle _ _ |- _ => inversion h as [?Cp [?Cr [?Cn ?Cc]]]
  end.
Tactic Notation "inv_cycle" "~" :=
  inv_cycle; auto_tilde.

(* Cycle length as the length of a list *)
Lemma cl_list: forall p l, Cycle p l -> length l = cl p.
Proof.
  intros. inv_cycle. unfold cl. symmetry. rewrites length_eq.
  auto using list_repr_inv_card.
Qed.

(* Cycle length as index *)
Lemma cl_index: forall p l i, Cycle p l -> (index l i = index (cl p:int) i).
Proof. intros. rew_list. forwards ->: cl_list. all: eauto. Qed.

(* Hint Rewrite xfor_simplify_inequality_lemma: rew_maths. *)

Ltac rew_cycle :=
  match goal with
  | h: Cycle ?p ?l |- _ =>
    (* Rewrite cycle indexes (workaround for [rewrite _ in *]) *)
    repeat match goal with
    | Hindex: context [index l ?i] |- _ => rewrite (cl_index i h) in Hindex
    | |-      context [index l ?i]      => rewrite (cl_index i h)
    end;
    (* Rewrite cycle list lengths *)
    repeat match goal with
    | Hlength: context [length l] |- _ => rewrite (cl_list h) in Hlength
    | |-       context [length l]      => rewrite (cl_list h)
    end
  end.

Lemma next_case: forall p l i,
  Cycle p l -> index l i ->
  (perm_ϕ p) l[i] = l[If i = cl p - 1 then 0 else i + 1].
Proof.
  introv H Hindex. rew_cycle. inv_cycle. case_if.
  * subst. rewrites~ Cc.
  * forwards: (Cn i). rew_index. auto.
Qed.

Lemma eq_index: forall p l i j,
  Cycle p l -> index l i -> index l j -> l[i] = l[j] -> i = j.
Proof.
  introv HC Hi Hj Heq. intros. inv_cycle. inv list_repr.
  forwards~ Hnateq: noduplicates_Nth_same_inv l.
  applys (>> Nth_of_index (Z.to_nat i)). rewrites~ Z2Nat.id. rew_index.
  rewrites Z2Nat.id. apply Heq. rew_index.
  applys (>> Nth_of_index (Z.to_nat j)). rewrites~ Z2Nat.id. rew_index.
  rewrites~ Z2Nat.id. rew_index.
  apply Z2Nat.inj. all: rew_index.
Qed.

Lemma cycle_domain: forall p l i,
  Cycle p l -> i ∈ (perm_D p) -> exists j, index l j /\ i = l[j].
Proof.
  intros. inv_cycle. destruct Cr as [_ Heq]. assert (mem i l). applys~ (Heq i).
  forwards~: (>> index_of_mem l i). eauto.
Qed.

Fixpoint nextn (p: perm) (n: nat) i :=
  match n with
  | 0%nat => i
  | S q   => (perm_ϕ p) (nextn p q i)
  end.

Lemma nextn_add: forall p n m i, nextn p n (nextn p m i) = nextn p (n + m) i.
Proof. induction n; simpl; auto. Qed.

Lemma nextn_domain: forall (p: perm) n i,
  Permutation (perm_D p) (perm_ϕ p) (perm_ψ p) ->
  i ∈ (perm_D p) -> nextn p n i ∈ (perm_D p).
Proof. induction n; introv Hperm ?; simpl. auto. lets~: (Pimgϕ Hperm). Qed.

Lemma nextn_case: forall p l i (n: nat),
  Cycle p l -> index l i -> index (cl p + 1) (n:int) ->
  nextn p n l[i] = l[If i + n > cl p - 1 then i + n - cl p else i + n].
Proof.
  induction n; intros.
  * case_if. rew_cycle. rew_index. simpl. auto with zarith.
  * simpl. rewrites~ IHn. rew_cycle. rewrites~ next_case. repeat case_if~.
    all: replace (S n:int) with (n+1); rew_index.
    all: replace (i+((n:int)+1)) with (i+(n:int)+1); rew_index.
    pick @eq (fun h => rewrite h). all: auto with zarith.
    rew_cycle. case_if. all: math.
Qed.

Lemma wrap: forall p l n i,
  Cycle p l -> i ∈ perm_D p -> (0 < n < cl p)%nat -> nextn p n i <> i.
Proof.
  introv HC Hi ?. intro Heq. sets x: (nextn p n i).
  asserts Hx: (x ∈ perm_D p). inv_cycle. apply~ nextn_domain.
  destruct (cycle_domain i HC Hi) as [mi [Hmi ->]].
  destruct (cycle_domain x HC Hx) as [mx [Hmx HxD]].
  assert (mi = mx). applys~ (>> eq_index HC). rewrite <- Heq, <- HxD. auto.
  subst. forwards Hnext: (>> nextn_case mx n HC). 1, 2: rew_cycle; rew_index.
  rew_cycle. case_if in Hnext.
  assert (mx = mx + n - cl p). apply (eq_index HC); rew_cycle; rew_index.
  rew_index.
  assert (mx = mx + n). apply (eq_index HC); rew_cycle; rew_index. rew_index.
Qed.

Definition cycle_part (p: perm) start (len: nat) :=
  \set{ i | exists (n: nat), (n < len)%nat /\ i = nextn p n start }.

Lemma add_substract: forall (x y: int), x + (y - x) = y.
Proof. math. Qed.
Hint Rewrite add_substract: rew_maths.

Lemma cycle_cover: forall p l start,
  Cycle p l -> start ∈ (perm_D p) -> cycle_part p start (cl p) = perm_D p.
Proof.
  introv HC Hstart. unfolds cycle_part. rew_set. intros x. split; intros Hx.
  * rew_set in Hx. destruct Hx as [n [? ?]]. subst. inv_cycle.
    apply~ nextn_domain.
  * destruct (cycle_domain x HC Hx) as [i [? ?]]. rew_set.
    destruct (cycle_domain start HC Hstart) as [j [? ?]]. subst.
    puts dist: (If j <= i then Z.to_nat (i - j) else Z.to_nat (i - j + cl p)).
    exists dist. split. subst dist. case_if.
    (* TODO: Inegality equivalence: a,b ≥ 0 -> (a≤b)%N <-> (a≤b)%Z (hard!) *)
    { rew_cycle. rew_index. admit. }
    { rew_cycle. rew_index. admit. }
    rewrites~ nextn_case; rew_cycle. case_if. all: subst dist; case_if.
    all: rewrite ? Z2Nat.id in *; rew_index. auto with zarith.
Qed.

(* @len is the number of elements already visited *)
Lemma cycle_step: forall p l start (len: nat),
  Cycle p l -> start ∈ perm_D p -> (len < cl p)%nat ->
  nextn p len start ∉ cycle_part p start len.
Proof.
  introv HC Hstart Hlen. intro Hin. unfolds cycle_part. rew_set in Hin.
  destruct Hin as [n [Hn Heq]]. symmetry in Heq.
  replace len with (len - n + n)%nat in Heq; try math.
  forwards~ Hneq: (@wrap p l (len - n) (nextn p n start) HC).
  inv_cycle. apply~ nextn_domain. math. rewrites~ nextn_add in Hneq.
Qed.

Lemma cycle_end: forall p l start,
  Cycle p l -> start ∈ perm_D p -> nextn p (cl p) start = start.
Proof.
  introv HC Hstart. destruct (cycle_domain start HC Hstart) as [n [? ->]].
  rewrites~ nextn_case. case_if. auto with zarith. rew_index.
Qed.



(** Cover problem objects **)

Parameter A: list (list bool).
Parameter n m: int.

Definition n_nat := Z.to_nat n.
Definition m_nat := Z.to_nat m.

(* CoverInv: constraints on valid cover_problem instances
   @R    is the set of active rows
   @C    is the set of active columns
   @head is the heading list, made of C_i and h
   @pc   is the list of pointers cols[0]..cols[m-1]
   @pr   is the list of pointers rows[0]..rows[n-1]
   @size is the helper for the S heuristic; size[c] = card (choices M c) *)
Record CoverInv R C (head: perm) pc pr (size: list int) := {

  (* The underlying instance is a valid matrix *)
  CMatrix:  Matrix A n m R C;

  (* The dimensions of the matrix are represented *)
  Clenhead: perm_n head = m + 1;
  Clensize: length size = m;
  Clenpc:   length pc = m;
  Clenpr:   length pr = n;
  Cwidth:   Forall (fun p => perm_n p = m + 1) pr;
  Cheight:  Forall (fun p => perm_n p = n + 1) pc;

  (* The permutations represent the matrix contents on domain R × C *)
  CdomC:    forall c r, (c-1) ∈ C ->
              r ∈ perm_D pc[c-1] = ((r-1) ∈ R /\ A[r-1][c-1] = true \/ r = 0);
  CdomR:    forall r c, index n (r-1) ->
              c ∈ perm_D pr[r-1] = ((c-1) ∈ C /\ A[r-1][c-1] = true);

  (* Our permutations are cycles *)
  Ccyclec:  Forall (fun p => exists l, Cycle p l) pc;
  Ccycler:  Forall (fun p => exists l, Cycle p l) pr;

  (* The headings and the stored size are correct *)
  Chead:    forall x, x ∈ perm_D head = (x = 0 \/ (x-1) ∈ C);
  Csize:    forall c, c ∈ C -> size[c] = (card (perm_D pc[c]): int);
}.

Tactic Notation "inv_cover" :=
  pick CoverInv (fun h =>
  inversion h as [CM Clh Cls Clc Clr Cw Ch CC CR Ccc Ccr Chd Csz]).
Tactic Notation "inv_cover" hyp(h) :=
  inversion h as [CM Clh Cls Clc Clr Cw Ch CC CR Ccc Ccr Chd Csz].

(* Cover: representation predicate for objects of type [cover_problem]
   This definition mainly introduces intermediate pointers, all meaningful
   invariants are in CoverInv. *)
Definition Cover head pc pr (size: list int) cp :=
  Hexists cols rows,
  Hexists phead pcols prows psize,
    cp ~> `{
      head' := phead;
      cols' := pcols;
      rows' := prows;
      size' := psize
    } \*
    phead ~> TPerm head \*
    pcols ~> Arrayof TPerm cols (make_map cols pc) \*
    prows ~> Arrayof TPerm rows (make_map rows pr) \*
    psize ~> Array size \*
    \[ noduplicates cols /\ noduplicates rows ] \*
    \[ length cols = m /\ length rows = n ].

(* Folding and unfolding lemmas for this record *)

Lemma Cover_open: forall cp head pc pr size,
  cp ~> Cover head pc pr size ==>
  Hexists cols rows,
  Hexists phead pcols prows psize,
    cp ~> `{
      head' := phead;
      cols' := pcols;
      rows' := prows;
      size' := psize
    } \*
    phead ~> TPerm head \*
    pcols ~> Arrayof TPerm cols (make_map cols pc) \*
    prows ~> Arrayof TPerm rows (make_map rows pr) \*
    psize ~> Array size \*
    \[ noduplicates cols /\ noduplicates rows ] \*
    \[ length cols = m /\ length rows = n ].
Proof. intros. xunfolds~ Cover. Qed.

Lemma Cover_close:
  forall cp head pc pr size cols rows,
  forall phead pcols prows psize,
    cp ~> `{
      head' := phead;
      cols' := pcols;
      rows' := prows;
      size' := psize
    } \*
    phead ~> TPerm head \*
    pcols ~> Arrayof TPerm cols (make_map cols pc) \*
    prows ~> Arrayof TPerm rows (make_map rows pr) \*
    psize ~> Array size \*
    \[ noduplicates cols /\ noduplicates rows ] \*
    \[ length cols = m /\ length rows = n ] ==>
  cp ~> Cover head pc pr size.
Proof. intros. xunfolds~ Cover. Qed.

Hint Extern 1 (RegisterOpen (Cover _ _ _ _))  => Provide Cover_open.
Hint Extern 1 (RegisterClose (record_repr _)) => Provide Cover_close.

(* Extracting invariants from a folded Cover *)

Lemma cover_inv_headperm: forall head pc pr size cp,
  cp ~> Cover head pc pr size ==+>
  \[ Permutation (perm_D head) (perm_ϕ head) (perm_ψ head) ].
Proof.
  intros. xopen cp. xpull ;=> cols rows phead pcols prows psize ? [? ?].
  xchange (>> perm_inv_permutation phead). rewrite TPerm_unfold. eauto.
  hpull. intros. xclose~ cp. rewrite TPerm_unfold. auto.
Qed.

(* Other lemmas *)

Lemma cover_row_domain: forall R C (M: Matrix A n m R C) head pc pr size r c,
  CoverInv R C head pc pr size -> index n (r-1) ->
  c ∈ perm_D pr[r-1] = (c-1) ∈ elements M (r-1).
Proof. intros. inv_cover. forwards~ ->: CR r c. Qed.

(* This lemma is non-trivial; what it means is that when row r is active,
   necessarily all the columns it covers are also active, so [elements M r]
   never changes. *)
Lemma cover_R_irrelevance:
  forall R C R' C' head pc pr size r,
  forall (M: Matrix A n m R C) (M': Matrix A n m R' C'),
  CoverInv R C head pc pr size ->
  r ∈ R -> r ∈ R' ->
  elements M r = elements M' r.
Admitted.



(** Array bounds and sizes in Cover records **)

Lemma bound_C: forall R C c,
  Matrix A n m R C -> c ∈ C -> index m c.
Proof. introv M. inv_matrix~ M. Qed.

Lemma bound_R: forall R C r,
  Matrix A n m R C -> r ∈ R -> index n r.
Proof. introv M. inv_matrix~ M. Qed.

Lemma bound1_C: forall R C c,
  Matrix A n m R C -> (c - 1) ∈ C -> index (m + 1) c.
Proof. intros. forwards: bound_C; eauto. Qed.

Lemma bound1_R: forall R C r,
  Matrix A n m R C -> (r - 1) ∈ R -> index (n + 1) r.
Proof. intros. forwards: bound_R; eauto. Qed.

Lemma size_pr: forall R C head pc pr size r,
  CoverInv R C head pc pr size -> index n (r - 1) -> perm_n pr[r - 1] = m + 1.
Proof. intros. inv_cover. applys (>> Forall_index_inv Cw). rew_index. Qed.

Lemma size_pc: forall R C head pc pr size c,
  CoverInv R C head pc pr size -> index m (c - 1) -> perm_n pc[c - 1] = n + 1.
Proof. intros. inv_cover. applys (>> Forall_index_inv Ch). rew_index. Qed.



(** Operations on permutations and instances **)

(* This is the part where we 'calculate' the dancing steps and the solutions
   inside Coq, to carry on the proof. *)

Definition hide '((n, D, ϕ, ψ): perm) (y: int): perm :=
  (n, D \-- y, ϕ[ψ y := ϕ y], ψ[ϕ y := ψ y]).

Definition show '((n, D, ϕ, ψ): perm) (y: int): perm :=
  (n, D \++ y, ϕ[ψ y := y], ψ[ϕ y := y]).

(* Cover row [r] in permutation list [pc] *)
Definition cover_one_row pc r :=
  let f p := If r ∈ (perm_D p) then hide p r else p in
  LibList.map f pc.

(* Cover row [r] in  [pc] for all columns between [first_c] and [c] *)
Definition cover_one_row_part pc r first_c c :=
  let f i p := If first_c <= i < c /\ r ∈ (perm_D p) /\ r <> 0
               then hide p r else p in
  enumerate_map f pc.

(* Cover all rows from [rs] in permutation list [pc] *)
Definition cover_rows rs pc :=
  List.fold_left cover_one_row rs pc.

(* Rows before [r] that are available to cover column [c] in matrix [M] *)
Fixpoint choices_before R C (M: Matrix A n m R C) c r (p: nat): list int :=
  match p with
  | 0%nat => nil
  | S q =>
    let i := (p: int) in
    let start := choices_before M c r q in
    If A[i][c] = true /\ (i < r \/ r = 0) then start & i else start
  end.

Definition cover_row_size (pc: list perm) r size :=
  let f i n := If r ∈ (perm_D pc[i]) then n - 1 else n in
  enumerate_map f size.

Definition cover_row_size_part (pc: list perm) r first_c c size :=
  let f i n := If first_c <= i < c /\ r ∈ (perm_D pc[i]) /\ r <> 0
               then n - 1 else n in
  enumerate_map f size.

Definition cover_rows_size rs pc size :=
  List.fold_left (fun size r => cover_row_size pc r size) rs size.

Lemma length_cover_rows_size: forall rs pc size,
  length (cover_rows_size rs pc size) = length size.
Proof.
  induction rs; intros.
  * unfold cover_rows_size. simpl. auto.
  * simpl. rewrite IHrs. unfold cover_row_size. rew_list~.
Qed.



(** [without] functions **)

Lemma without_head_spec:
  forall f RetType (ret: RetType),
  forall cp R C (c: int) head pc pr size,

  CoverInv R C head pc pr size ->
  (c - 1) ∈ C ->

  (app f [cp]
    INV  cp ~> Cover (hide head c) pc pr size
    POST \[= ret])
  ->
  app without_head [cp c f]
    INV  cp ~> Cover head pc pr size
    POST \[= ret].
Proof.
  introv HC Hdom. xcf. inv_cover.
  xchange (>> cover_inv_headperm cp). xpull. intros Hperm.
  xopen cp. xpull ;=> cols rows phead pcols prows psize Hdup [Hlc Hlr].

  (* Hide an element from the head *)
  xapps. xapp. rewrite Chd. intuition assumption. rewrites~ TPerm_unfold.

  (* Close the record to get the head change *)
  intros Hupd. xclose (>> cp (hide head c)). rewrite TPerm_unfold.
  destruct head as [[[hn hD] hϕ] hψ]. simpl. all: auto.
  clear cols rows phead pcols prows psize Hdup Hlc Hlr.

  (* Execute the subfunction *)
  xapps.

  (* Show the element again *)
  xopen cp. xpull ;=> cols rows phead pcols prows psize ? [Hlc Hlr].
  xapps. assert (c ∈ perm_D head) by rewrites~ Chd. inversion Hperm.
  destruct head as [[[hn hD] hϕ] hψ]. simpl in Hupd. xapp.
  assert (index (m + 1) c). inv_matrix CM. lets: (Mc (c-1) Hdom). rew_index.
  simpl in Clh. eauto.
  auto. 1, 2: intuition apply~ set_in_out. 1, 2: rew_update.

  (* Close the record and find the original head *)
  xclose~ (>> cp ((hn, hD, hϕ, hψ): perm)).
  forwards [Hrev1 Hrev2]: (revert_update c Hperm). auto.
  rewrites <- Hrev1. rewrites~ <- Hrev2. rewrites~ <- set_add_one_remove_same.

  (* Forward the return value *)
  xrets~.
Qed.

(* TODO: Do the other [without] function *)

Hint Extern 1 (RegisterSpec without_head) => Provide without_head_spec.



(** Covering elements **)

(* The following definitions represent the intermediate states of R, C and size
  during covering. *)

Definition temp_R R C (M: Matrix A n m R C) r c :=
  R \- \set{ r' ∈ choices M c | r' <= r \/ r = 0 }.

Lemma temp_R_zero: forall R C M r c,
  r = 0 -> @temp_R R C M r c = R \- choices M c.
Proof. intros. unfold temp_R. rew_set. intros. split; rew_set; tauto. Qed.

Lemma finalize_pc: forall pc rs r first_c c,
  r=0 -> cover_one_row_part (cover_rows rs pc) r first_c c = cover_rows rs pc.
Proof.
  intros. unfold cover_one_row_part. applys (>> eq_of_extens). rew_list~.
  introv Hind. rewrites (>> read_enumerate_map). rew_index.
  case_if; intuition auto.
Qed.

Lemma finalize_size: forall pc rs r first_c c size,
  r = 0 -> cover_row_size_part pc r first_c c (cover_rows_size rs pc size) =
    cover_rows_size rs pc size.
Proof.
  intros. unfold cover_row_size_part. applys (>> eq_of_extens). rew_list~.
  introv Hind. rewrites (>> read_enumerate_map). rew_index.
  case_if; intuition auto.
Qed.

(* This proof is too difficult right now, the permutations need to be replaced
   by cycles and more automation is needed. *)

Lemma cover_element_spec:
  forall R C (M: Matrix A n m R C) head pc pr size,
  forall cp f P Q r (c: int) first_c,

  CoverInv R C head pc pr size ->
  (first_c - 1) ∈ C -> (c - 1) ∈ C -> (r - 1) ∈ R ->

  let rs :=
    choices_before M c r n_nat in
  let part_pc :=
    cover_one_row_part (cover_rows rs pc) r first_c c in
  let full_pc :=
    cover_rows rs pc in
  let part_size :=
    cover_row_size_part pc r first_c c (cover_rows_size rs pc size) in
  let full_size :=
    cover_rows_size rs pc size in

  (app f [tt]
    PRE    cp ~> Cover head full_pc pr full_size \* P
    POST # cp ~> Cover head full_pc pr full_size \* Q)
  ->
  app cover_element [f cp r c first_c]
    PRE    cp ~> Cover head part_pc pr part_size \* P
    POST # cp ~> Cover head part_pc pr part_size \* Q.
Proof.
(*   introv HC. inv_cover.
    pose (measure := If r = 0 then 0 else r * m + (c - first_c)).
  induction_wf IWF: (downto 0) measure.
  xcf. xrets. xif. xapp.

  (* When iteration stops, everything is covered *)
  1, 2: unfold part_pc, part_size, full_pc, full_size.
  1, 2: rewrites~ finalize_pc; rewrites~ finalize_size. hsimpl.

  (* Decrement size *)

  xopen cp. xpull;=> cols rows phead pcols prows psize [Hcols Hrows]. xapps.
  inv_matrix CM. assert (Lcm1: index m (c - 1)) by auto.
  assert (Lpart_size: index part_size (c - 1)).
  rewrite index_eq_index_length.
  unfold part_size, cover_row_size_part. rewrite length_enumerate_map.
  rewrite length_cover_rows_size. rewrites~ Cls.

  (** Prove the specification of the sub-function Fcover_more **)

  xapps~. xapps. xapp~.
  xfun (fun f =>
    let c' := (perm_ϕ pr[r - 1]) c in
    let more_pc :=
      cover_row_part (cover_rows rs pc) r first_c c' in
    let more_size :=
      cover_row_size_part pc r first_c c' (cover_rows_size rs pc size) in
    app f [tt]
      PRE    cp ~> Cover head more_pc pr more_size \* P
      POST # cp ~> Cover head more_pc pr more_size \* Q)
  as Fcover_more.

{
  xopen cp. xpull ;=> cols' rows' phead' pcols' prows' psize' [Hcols' Hrows'].
  xapps. xapps_spec arrayof_get_spec. typeclass. apply index_of_index_length.
  rewrites~ Hrows'.
  xapps. forwards : @Forall_index_inv (r - 1) Cw. apply index_of_index_length.
  rewrites~ Clr. instantiate (X1 := m + 1). auto.

  (* ; *)


  (** ... **)

(*   xapps. xapps_spec arrayof_get_spec. typeclass. apply index_of_index_length.
  rewrites~ Hcols. xclose~ cp.

  (* Apply [without] to hide a single element *)

  xapps P Q. inv_cover HC_base. rewrites (>> Forall_index_inv (c - 1) Ch).
  rew_index. assert (index n (r - 1)) by inv_matrix~ CM. rew_index.
  inv_cover HC_base. assert (r ∈ shift R). unfold shift. rew_set. left.
  eapply LibFun.in_image_prove. eauto. math. eapply incl_inv; eauto.
  applys~ (>> Forall_index_inv Cd).
 *) *)
Admitted.



(** Covering columns **)

(* This specification is simpler than [cover_element] because the intermediate
   states are hidden by [cover_element]. *)

Lemma cover_column_spec:
  forall R C (M: Matrix A n m R C) head pc pr size,
  forall cp f RetType (ret: RetType) (c: int),

  CoverInv R C head pc pr size ->
  (c - 1) ∈ C ->

  (forall head' pc' pr' size',
    CoverInv (R \- choices M c) (C \-- c) head' pc' pr' size' ->
    app f [tt]
      INV  cp ~> Cover head' pc' pr' size'
      POST \[= ret])
  ->
  app cover_column [cp c f]
    INV  cp ~> Cover head pc pr size
    POST \[= ret].
Proof.
  introv HC. xcf.

  (* TODO: Prove the specification of the subfunction Fcovers (needs a cleaner
     TODO: spec for [cover_element] *)

  xfun (fun fcovers =>
    app fcovers [cp]
      INV  cp ~> Cover (hide head c) pc pr size
      POST \[= ret])
  as Fcovers. admit.

  xapp. eauto. auto. xapp.
Admitted.

Hint Extern 1 (RegisterSpec cover_column) => Provide cover_column_spec.



(** Selecting rows **)

(* Here again the following lemmas represent the intermediate state during
   covering. I have annotated the argument range because of an ambiguity:
   - C/R elements are 1..n, as permutations in the OCaml source, because 0 is
     used for special purposes (C_i and h)
   - Coq Lists indices are 0..n-1 *)

(* @r @first in 1..n
   Returns in 0..n-1 *)
Definition elements_between (pr: list perm) r first (len: nat) :=
  LibFun.image (fun c => c-1) (cycle_part pr[r-1] first len).

(* @r @c @first is 1..n
   Sets are heterogeneous *)
Lemma in_elements_between: forall pr r first len c,
  (c-1) ∈ elements_between pr r first len = c ∈ cycle_part pr[r-1] first len.
Proof.
  intros. unfolds elements_between. apply prop_ext. split.
  * intros. forwards Himg: LibFun.in_image_inv. eauto. destruct Himg as [x ?].
    asserts ->: (x = c). math. intuition.
  * intros. eapply LibFun.in_image_prove. eauto. math.
Qed.

(* @r @first @r' @c' is 1..n
   Returns in 0..n-1 *)
Definition collisions_between R C (M: Matrix A n m R C) pr r first (len:nat) :=
  \set{ r' ∈ R | exists c',
    c' ∈ elements_between pr r first len /\ A[r'][c'] = true }.

(* @r @first is 1..n
   Sets are in domain 0..n-1 *)
Lemma finalize_elements_between:
  forall R C (M: Matrix A n m R C) head pc pr size r first,
  CoverInv R C head pc pr size -> index pr (r-1) -> first ∈ perm_D pr[r-1] ->
  elements_between pr r first (cl pr[r-1]) = elements M (r-1).
Proof.
  intros. inv_cover. rew_set. intros x. replace x with (x+1-1); try math.
  rewrite in_elements_between.
  forwards Hcyc: (>> Forall_index_inv Ccr). eauto. destruct Hcyc as [l ?].
  rewrites (>> cycle_cover). eauto. auto.
  unfolds elements. rew_set. forwards ->: CR. rew_index. tauto.
Qed.

(* @r @first is 1..n
   Sets are in domain 0..n-1 *)
Lemma finalize_R:
  forall R C (M: Matrix A n m R C) head pc pr size r first,
  CoverInv R C head pc pr size -> index pr (r-1) -> first ∈ perm_D pr[r-1] ->
  collisions_between M pr r first (cl pr[r-1]) = collisions M (r-1).
Proof.
  introv HC ? ?. unfolds collisions_between, collisions. rew_set. intros r'.
  forwards~ ->: (finalize_elements_between M r first HC).
  split; rew_set; intros [Hr' [c' Hc']].
  * intuition auto. exists c'. intuition auto.
  * intuition auto. exists c'. intuition auto.
Qed.

Lemma finalize_C:
  forall R C (M: Matrix A n m R C) head pc pr size r first,
  CoverInv R C head pc pr size -> index pr (r-1) -> first ∈ perm_D pr[r-1] ->
  C \- elements_between pr r first (cl pr[r-1]) = C \- elements M (r-1).
Proof. intros. forwards ->: finalize_elements_between. all: eauto. Qed.

(* This proof is simpler than [cover_element] but still needs to iterate. This
   is helped by the [finalize] lemmas that indicate when iteration stops. *)

Lemma select_row_spec:
  forall R C (M: Matrix A n m R C) head pc pr size,
  forall cp f RetType (ret: RetType) (r first c: int) (len: nat),

  let part_R :=
    R \- collisions_between M pr r first len in
  let part_C :=
    C \- elements_between pr r first len in

  CoverInv part_R part_C head pc pr size ->
  len < cl pr[r-1] ->
  c = nextn pr[r-1] len first ->
  (r-1) ∈ R -> (first-1) ∈ elements M (r-1) -> (c-1) ∈ C ->

  (forall head' pc' pr' size',
    CoverInv (R \- collisions M r) (C \- elements M r) head' pc' pr' size' ->
    app f [cp]
      INV  cp ~> Cover head' pc' pr' size'
      POST \[= ret])
  ->
  app select_row [cp r first c f]
    INV  cp ~> Cover head pc pr size
    POST \[= ret].
Proof.
  introv HC Hnextn ? ? Hfirst ? Hspec. xcf. xfun as Fcolumn.

  (* Prove the hypotheses for [cover_column] *)
  assert ((c - 1) ∈ (C \- elements_between pr r first len)). rew_set. split.
  auto. intro Hin. rewrite in_elements_between in Hin.
  inv_cover.
  { forwards Hcyc: (>> Forall_index_inv (r-1) Ccr). rew_list. rewrite Clr.
     applys~ (>> bound_R M). destruct Hcyc as [l Hcyc].
     forwards: (@cycle_step pr[r-1] l first len Hcyc).
     (* TODO: Want to use M (Hfirst) but HC is about reduced matrix CM!
        TODO: Prove and use cover_R_irrelevance. *)
     (* rewrites (>> cover_row_domain HC). applys~ (>> bound_R M).
        forwards: (@CR r first). applys~ (>> bound_R M). auto.
        apply Hfirst. *)
     admit. admit.
     auto.
  }
  xapp; eauto.

  (* Execute [Fcolumn] as the continuation of [cover_column] *)
  introv HC'. xapp.
  xopen cp. xpull ;=> cols rows phead pcols prows psize ? [Hlc Hlr].
  xapps. xapps_spec arrayof_own_spec. rew_list. rewrite Hlr. inv_matrix~ M.

  (* Move on to next column *)
  xapps. apply~ (bound1_C c M).
  forwards <-: (size_pr r HC'). apply~ (bound_R (r - 1) M).
  rewrite read_make_map. rewrite TPerm_unfold. eauto. intuition.
  rew_list. rewrite Hlr. inv_matrix~ M. inv_cover HC'.
  rew_list. rewrite Clr. inv_matrix~ M.

  (* Prove that we've made a step in terms of R and C *)
  sets c': (perm_ϕ pr'[r-1] c).
  (* TODO: Rewrite in terms of length, not end column (I changed the definition
     TODO: of cycles since this part was written) *)
  (* assert (part_C M r first_c c \-- c = part_C M r first_c c').
     { unfold part_C in H3. rew_set in H3.
       unfold part_C. rew_set. split. intros J. rew_set in *.
       destruct J as [[J K] L]. split. auto. intro. tests: (x ∈ elements M r).
       assert (first_c - 1 <= x < (c - 1) \/ c = first_c).
     } *)

  (* Test whether [next cp.rows.(r - 1) c = first_c] *)
  xrets. xif.

  (* End case: we've reached first_c, time to roll! *)
  xapp_spec Hspec.

  (* TODO: Do this proof by induction on [card C] and conclude here using the
     induction hypothesis *)
Admitted.
