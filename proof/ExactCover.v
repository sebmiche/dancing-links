(*
**  ExactCover.v: Abstract proof of the backtracking method for Exact Cover.
**
**  This script represents an abstract cover problem as a matrix A of size n*m
**  with two sets R and C that mark the active rows and columns of the matrix.
**  This representation has two main advantages: it avoids modifying the matrix
**  and it's closer to the dancing-links-way of solving the problem.
**
**  The main result of this script is [backtracking_step], it proves that:
**  - If the matrix is empty, there is exacly one solution: the empty set.
**  - Otherwise, the user can select any active column c. All the solutions
**    will then consist of a row r that cover c plus a solution of the reduced
**    instance where all columns and rows that intersect r are hidden.
**
**  Additionally, [branch_diff] proves that solutions obtained after choosing a
**  different r to cover the user-chosen column c will be distinct.
*)

Set Implicit Arguments.

Require Import TLC.LibInt.
Require Import TLC.LibSet.
Require Import TLC.LibListZ.
Require Import TLC.LibTactics.
Require Import TLC.LibCore.
Require Import Extensions.

Open Scope set_scope.

(** A matrix with an active subset of rows and columns **)

(* Matrix predicate: Characterizes well-formed instances of the cover problem
   @A is a boolean matrix where columns are elements and rows are subsets
   @n is the height of the matrix
   @m is the width of the matrix (only use 'p' for CFML pointers)
   @R is the set of active (uncovered, remaining) rows
   @C is the set of active (uncovered, remaining) columns *)
Record Matrix (A: list (list bool)) (n m: int) (R C: set int) := {
  Mheight : length A = n;
  Mwidth  : Forall (fun r => length r = m) A;
  (* TODO: Use the [foreach] predicate from TLC? *)
  Minv    : forall r, r ∈ R -> exists c, c ∈ C /\ A[r][c] = true;
  Mrows   : forall r, r ∈ R -> index n r;
  Mcols   : forall c, c ∈ C -> index m c;
}.

(* truly_empty: if the matrix has no columns then it has no rows *)
Lemma truly_empty: forall A n m R C (M: Matrix A n m R C),
  C = \{} -> R = \{}.
Proof. introv ->. apply is_empty_prove. intros r Hr. destruct~ (Minv M r). Qed.

Tactic Notation "inv_matrix" hyp(M) :=
  inversion M as [Mh Mw Mi Mr Mc].
Tactic Notation "inv_matrix" "~" hyp(M) :=
  inv_matrix M; auto_tilde.

(** Meaningful sets of rows and columns, solutions to the cover problem **)

(* choices: Set of rows that can be used to cover column c *)
Definition choices A n m R C (M: Matrix A n m R C) (c: int) :=
  \set{ r ∈ R | A[r][c] = true }.

(* elements: Set of (active) columns covered by row r *)
Definition elements A n m R C (M: Matrix A n m R C) (r: int) :=
  \set{ c ∈ C | A[r][c] = true }.

(* collisions: Set of rows that have common columns with r *)
Definition collisions A n m R C (M: Matrix A n m R C) (r: int) :=
  \set{ r' ∈ R | exists c, c ∈ elements M r /\ A[r'][c] = true }.

Tactic Notation "rew_elements" "in" "*" :=
  repeat match goal with
  | h : context[elements _ _] |- _ => unfold elements in h; rew_set in h
  end.

(* solution predicate: Characterizes solutions to a cover problem instance
   @M is the problem instance
   @S is the set of rows whose sum is a row of 1's *)
Definition solution A n m R C (M: Matrix A n m R C) (S: set int) :=
  S ⊆ R /\ forall c, c ∈ C -> exists! s, s ∈ S /\ A [s][c] = true.

(* solution_no_collisions: Equivalent formulation of the solution property *)
Lemma solution_no_collisions: forall A n m R C (M: Matrix A n m R C)
  (S: set int) (r s: int),
  solution M S -> r ∈ S -> s ∈ S -> s ∈ collisions M r -> r = s.
Proof.
  introv [? Hcov] ? ? [? [c [[Hc ?] ?]]]. destruct (Hcov c Hc) as[? [? Huniq]].
  rewrite (Huniq r). rewrite (Huniq s). all: intuition auto.
Qed.

(* solution_reduce: A solution can be reduced by removing just one element *)
Lemma solution_reduce: forall A n m R C (M: Matrix A n m R C)
  (S: set int) (r: int),
  solution M S -> r ∈ S -> S \-- r = S \- collisions M r.
Proof.
  intros. apply in_extens. intros. rewrite ! in_remove_eq.
  intuition idtac; intro Hin.
  * inversion Hin as [_ [c [[Hc ?] ?]]]. inv solution as [? Huniq].
    destruct (Huniq c Hc) as [s [_ Hs]].
    assert (x = r). rewrite (Hs x). rewrite (Hs r). all: auto.
  * assert (r ∈ collisions M r). inv solution.
    assert (Hr: r ∈ R) by (rew_set in *; auto). constructor~.
    inv_matrix M. destruct (Mi r Hr) as [c ?]. exists c. intuition auto.
    constructor~. auto.
Qed.

(** End case: empty solutions **)

Lemma backtracking_end: forall A n m R C (M: Matrix A n m R C) (S: set int),
  C = \{} -> solution M S <-> S = \{}.
Proof.
  introv HC. split; intros.
  * lets : (truly_empty M HC). inv~ solution.
  * subst. constructor~.
Qed.

(** Reduced matrix **)

(* reduced_{rows,cols}: Reduced sets of rows and columns after removal of row r
   and all rows and columns that intersect it *)
Definition reduced_rows A n m R C (M: Matrix A n m R C) (r: int) :=
  R \- collisions M r.
Definition reduced_cols A n m R C (M: Matrix A n m R C) (r: int) :=
  C \- elements M r.

(* Destructing (_ \- _) exposes the function-based definition of sets; rewrite
   [in_remove_eq] instead *)
Tactic Notation "rew_reduced" :=
  unfold reduced_cols, reduced_rows;
  try rewrite in_remove_eq.
Tactic Notation "rew_reduced" "in" "*" :=
  repeat match goal with
  | h : context[reduced_rows _ _] |- _ =>
    unfold reduced_rows in h; rewrite ? in_remove_eq, ? incl_remove_eq in h
  | h : context[reduced_cols _ _] |- _ =>
    unfold reduced_cols in h; rewrite ? in_remove_eq, ? incl_remove_eq in h
  end.

(* reduced_matrix: The reduced problem is still a valid instance.
   The large block proves that the remaining rows are not empty. They were
   originally not, and if they had lost columns they would have disappeared. *)
Lemma reduced_matrix: forall A n m R C (M: Matrix A n m R C) (r: int),
  Matrix A n m (reduced_rows M r) (reduced_cols M r).
Proof.
  intros. inv_matrix M. constructor~.
  { intros s Hs. rew_reduced in *. destruct Hs as [Hs Hcoll].
    destruct (Mi s Hs) as [c Hc].
    exists c. rew_reduced. repeat split. intuition. intro.
    assert (s ∈ collisions M r). constructor~. exists c. all: intuition. }
  all: intros; rew_reduced in *; intuition auto.
Qed.

(** Relations between solutions to M and solutions to the reduced matrix **)

(* solution_induce: Solutions to M contain solutions to all sub-problems *)
Lemma solution_induce: forall A n m R C (M: Matrix A n m R C)
  (S: set int) (r: int),
  solution M S -> r ∈ S -> solution (reduced_matrix M r) (S \-- r).
Proof.
  introv Hsol Hr. inversion Hsol as [HS Hcov].
  rewrite (solution_reduce r Hsol Hr). constructor.
  { rew_reduced. apply~ set_incl_remove. }
  { intros c Hc. rew_reduced in *. destruct Hc as [Hc Hel].
    destruct (Hcov c Hc) as [s [Hs Huniq]]. exists s; constructor.
    * rew_set. intuition idtac. assert (c ∈ elements M s) by constructor~.
      assert (Heq: r = s) by (apply (solution_no_collisions Hsol); auto).
      rewrites~ Heq in *.
    * introv [Hy ?]. apply Huniq. split. apply Hy. auto. }
Qed.

(* solution_extend: A solution to the problem reduced using row r can be
   extended into a solution of the whole problem by adding r *)
Lemma solution_extend: forall A n m R C (M: Matrix A n m R C)
  (S: set int) (r: int),
  r ∈ R ->
  solution (reduced_matrix M r) S -> solution M (S \++ r).
Proof.
  intros. inv solution as [HS Hsol]. rew_reduced in *. constructor.
  { rew_set. intros. rew_set in *. intuition subst~. }
  { introv Hc. tests HC : (c ∈ elements M r).
    * inversion HC. rew_set in *. exists r. split. intuition.
      intros. tests~ : (y = r). unpack. strongset in *.
      assert (y ∈ collisions M r). constructor~. exists c. intuition auto.
      false. eauto.
    * assert (Hnot: c ∈ (C \- elements M r)). rew_set~.
      lets Huniq : Hsol c Hnot. destruct Huniq as [s [Hs Huniq]].
      exists s. split. intuition. intros. unpack. tests : (y = r).
      assert (c ∈ elements M r). constructor~. auto. strongset in *. auto. }
Qed.

(* backtracking_base: Let I be an instance of the cover problem, r a row and I'
   the reduced instance with respect to row r.
   Then solutions to I that contain row r and solutions to I' extended with row
   r are the same objects. *)
Lemma backtracking_base: forall A n m R C (M: Matrix A n m R C)
  (S: set int) (r: int),
  r ∈ R -> r ∈ S ->
  solution M S <-> solution (reduced_matrix M r) (S \-- r).
Proof.
  intros. split.
  * intros HS. apply~ solution_induce.
  * intros HS. replace S with ((S \-- r) \++ r). apply~ solution_extend.
    symmetry. apply~ set_add_one_remove_same.
Qed.

(** Backtracking step: relations between solutions from different branches **)

(* branch_diff: Choosing a different row r to cover a chosen column c will
   ultimately give different solutions *)
Lemma branch_diff: forall A n m R C (M: Matrix A n m R C)
  (S1 S2: set int) (c r1 r2: int),
  solution M S1 -> solution M S2 -> r1 ∈ S1 -> r2 ∈ S2 ->
  c ∈ elements M r1 -> c ∈ elements M r2 -> r1 <> r2 -> S1 <> S2.
Proof.
  intros. intro. inv solution as [_ Hsol]. rew_elements in *.
  assert (Hc: c ∈ C) by intuition. destruct (Hsol c Hc) as [s [_ ?]].
  assert (r1 = s) by intuition. assert (r2 = s) by intuition. auto.
Qed.

(* branch_exhaust: Branching for all rows r capable of covering column c is
   enough to get all the solutions *)
Lemma branch_exhaust: forall A n m R C (M: Matrix A n m R C)
  (S: set int )(c: int),
  c ∈ C ->
  solution M S <-> solution M S /\ exists r, r ∈ S /\ c ∈ elements M r.
Proof.
  introv Hc. split.
  * intros HS. intuition auto.
    inv solution as [_ Hsol]. destruct (Hsol c Hc) as [s [Hs Huniq]].
    exists s. intuition auto. constructor~.
  * intuition.
Qed.

(* backtracking_step: The solutions to I are exactly solutions to I' (wrt.
   some row r) extended with row r. *)
Lemma backtracking_step: forall A n m R C (M: Matrix A n m R C)
  (S: set int) (c: int),
  c ∈ C ->
  (* TODO: Reformulate in terms of r ∈ choices M c instead *)
  solution M S <-> exists r, r ∈ R /\ r ∈ S /\ c ∈ elements M r /\
                   solution (reduced_matrix M r) (S \-- r).
Proof.
  intros. rewrite~ (branch_exhaust M S c). split; intros HS.
  * destruct HS as [Hsol [r [? ?]]]. inversion Hsol. rew_set in *.
    assert (HS: solution M S /\ r ∈ S) by auto.
    rewrite (backtracking_base M S r) in HS. destruct HS as [Hred _].
    exists r. all: intuition auto.
  * destruct HS as [r (? & ? & ? & ?)].
    assert (HS: r ∈ S /\ solution (reduced_matrix M r) (S \-- r)) by auto.
    rewrite <- (backtracking_base M S r) in HS.
    intuition auto. exists r. all: auto.
Qed.
