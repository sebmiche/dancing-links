(*
**  LibPermutation.v: Abstract permutations.
*)

Set Implicit Arguments.

Require Import Stdlib.
Require Import TLC.LibInt.
Require Import TLC.LibSet.
Require Import TLC.LibTactics.
Require Import TLC.LibCore.
Require Import Extensions.

Open Scope set_scope.

Record Permutation (D: set int) (ϕ: int -> int) (ψ: int -> int) := {
  Pinvϕ: forall k, k ∈ D -> ψ (ϕ k) = k;
  Pinvψ: forall k, k ∈ D -> ϕ (ψ k) = k;
  Pimgϕ: forall k, k ∈ D -> ϕ k ∈ D;
  Pimgψ: forall k, k ∈ D -> ψ k ∈ D;
}.

(* Don't provide [k] since we already provide [k ∈ D]. *)
Arguments Pinvϕ [D] [ϕ] [ψ] p [k].
Arguments Pinvψ [D] [ϕ] [ψ] p [k].
Arguments Pimgϕ [D] [ϕ] [ψ] p [k].
Arguments Pimgψ [D] [ϕ] [ψ] p [k].

Implicit Types D: set int.
Implicit Types ϕ: int -> int.
Implicit Types ψ: int -> int.

(** Injectivity of permutations **)

Lemma Pinjϕ: forall D ϕ ψ (P: Permutation D ϕ ψ) i j,
  i ∈ D -> j ∈ D -> ϕ i = ϕ j -> i = j.
Proof.
  introv P Di Dj Heq.
  rewrites <- (>> Pinvϕ P Di).
  rewrites <- (>> Pinvϕ P Dj).
  fequal~.
Qed.

Lemma Pinjψ: forall D ϕ ψ (P: Permutation D ϕ ψ) i j,
  i ∈ D -> j ∈ D -> ψ i = ψ j -> i = j.
Proof.
  introv P Di Dj Heq.
  rewrites <- (>> Pinvψ P Di).
  rewrites <- (>> Pinvψ P Dj).
  fequal~.
Qed.

Arguments Pinjϕ [D] [ϕ] [ψ] P [i] [j].
Arguments Pinjψ [D] [ϕ] [ψ] P [i] [j].
