(*
**  Extensions.v: Various lemmas that could be added to TLC.
*)

Set Implicit Arguments.

Require Import Stdlib.
Require Import FunctionalExtensionality.

Require Import TLC.LibContainer.
Require Import TLC.LibFun.
Require Import TLC.LibInt.
Require Import TLC.LibListZ.
Require Import TLC.LibMap.
Require Import TLC.LibMonoid.
Require Import TLC.LibSet.
Require Import TLC.LibTactics.
Require Import TLC.LibCore.

(** Automation **)

Hint Extern 1 => false_invert.

(* Suggest adding typeclass_instances by default *)
(* Ltac auto_tilde := auto with wf typeclass_instances. *)

(** Logic **)

(* Is there any standard/predefined way of doing this? *)
Lemma fold_neq: forall A (x y: A), (x = y -> False) = (x <> y).
Proof. auto. Qed.

(** Arithmetic **)

(* When using case analysis on ints, the default inductive cases n = 0,
   n = Z.pos p and n = Z.neg p (p: positive) are not really helpful (mixing
   integer types is a hassle). [int_pos] and [int_neg] are used by some of the
   tactics below to turn theses cases into n = 0, n > 0 and n < 0 *)

Lemma int_pos: forall (i: int) (p: positive), i = Z.pos p -> (i > 0).
Proof. intros. lets : Zgt_pos_0 p. eauto with *. Qed.

Lemma int_neg: forall (i: int) (p: positive), i = Z.neg p -> (i < 0).
Proof. intros. lets : Zlt_neg_0 p. eauto with *. Qed.

(* minus_one_nat_eq_int: stumbled on this one (it should be automated really)
   This proof is 'difficult'; I could not avoid destructing n. *)
Lemma minus_one_nat_eq_int: forall (n: nat),
  n <> 0%nat  -> (n - 1)%I = (n - 1)%nat.
Proof. intros. destruct~ n. simpl. replace (n - 0)%nat with n; math. Qed.

(** Tactics **)

(* store: prove (a duplicate of) the goal and make it a hypothesis *)
Ltac store :=
  match goal with
  | |- ?g => assert g
  end.

(* pick: choose an instance of the provided predicate in the hypotheses and
         apply a continuation (François Pottier) *)

Ltac pick_core R k :=
  match goal with
  | h: R               |- _ => k h
  | h: R _             |- _ => k h
  | h: R _ _           |- _ => k h
  | h: R _ _ _         |- _ => k h
  | h: R _ _ _ _       |- _ => k h
  | h: R _ _ _ _ _     |- _ => k h
  | h: R _ _ _ _ _ _   |- _ => k h
  | h: R _ _ _ _ _ _ _ |- _ => k h
  end.

Tactic Notation "pick" constr(R) tactic(t) :=
  pick_core R t.

(* inv: invert a hypothesis chosen by pick *)
Ltac fp_invert h :=
  inversion h; clear h; try subst.
Tactic Notation "inv" constr(R) :=
  pick R fp_invert.
Tactic Notation "inv" constr(R) "as" intropattern(p) :=
  pick R ltac:(fun h => inversion h as p; clear h; try subst).
Tactic Notation "inv" "~" constr(R) :=
  inv R; auto_tilde.

(* rew_index: normalize index expressions and solve using omega *)
Tactic Notation "rew_index" :=
  rewrite ? index_eq_index_length, ? int_index_eq in *;
  rew_list in *;
  try math.
Tactic Notation "rew_index" "~" :=
  rew_index; auto_tilde.

(* Reasoning by case analysis on ints.
     case_eq (n: int).
     * admit.                  # here n = 0
     * case_pos. admit.        # provides n > 0
     * case_neg. admit.        # provides n < 0
   I don't know if it's possible to apply them automatically using some sugar
   around [case_eq]. *)

(* case_pos: turns [|- forall p, n = Z.pos p -> ...] into [n > 0 |- ...] *)
Ltac case_pos :=
  intros;
  match goal with
  | H : ?n = Z.pos ?p |- _ =>
    try rewrite <- H in *; apply int_pos in H; clear p
  end.

(* case_neg: turns [|- forall p, n = Z.neg p -> ...] into [n < 0 |- ...] *)
Ltac case_neg :=
  intros;
  match goal with
  | H : ?n = Z.neg ?p |- _ =>
    try rewrite <- H in *; apply int_neg in H; clear p
  end.

(** Lists **)

Hint Rewrite read_zero read_succ : rew_list.
Hint Rewrite map_nil map_cons : rew_list.

(* Is this one always desirable? *)
Hint Rewrite index_eq_index_length : rew_list.

(* index_cons: turn a non-zero index in (a :: l) into an index in l *)
Lemma index_cons: forall A (l: list A) (a: A) (i: int),
  i > 0 -> index (a :: l) i -> index l (i - 1).
Proof. intros. inv @index. rew_list in *. rew_index. Qed.

(* mem_of_index: proves that l[i] is in l *)
Lemma mem_of_index: forall A (H: Inhab A) (l: list A) (i : int),
  index l i -> mem l[i] l.
Proof.
  intros A H l. induction l. intros. inv @index. rew_list in *. math.
  introv Hindex. case_eq i.
  * intros. rew_list. apply mem_here.
  * case_pos. apply index_cons in Hindex. replace i with ((i - 1) + 1).
    rew_list. apply mem_next. apply IHl. rew_index. auto. math. auto.
  * intros. lets : Zlt_neg_0 p. rew_index.
Qed.

(* index_of_mem: proves that every i in l has an index (less trivial) *)
Lemma index_of_mem: forall A (H: Inhab A) (l: list A) (a : A),
  mem a l -> exists i, index l i /\ a = l[i].
Proof.
  introv Hmem. induction Hmem.
  { exists 0. split. split; rew_list; math. rew_list~. }
  { destruct IHHmem as [j [? Heq]]. exists (j + 1). split.
    rew_list; rew_index. rewrite read_cons_case. case_if. rew_index.
    rewrite Heq. assert (HH: j = j+1-1). auto with zarith. rewrites~ <- HH. }
Qed.

(* index_nil: for absurd goals. Can probably be used through auto *)
Lemma index_nil: forall A (l: list A) (k: int), index l k -> l <> nil.
Proof. intros. intros ->. inv @index. rew_list in *. math. Qed.

(* Forall_index_inv: another way to invert Forall (besides mem, cons, etc) *)
Lemma Forall_index_inv: forall A (H: Inhab A) (l: list A) P i,
  Forall P l -> index l i -> P l[i].
Proof. introv Hf ?. apply (Forall_mem_inv Hf). apply~ mem_of_index. Qed.

(* mem_map_inv: every i in [map f l] is [f j] for some j in l *)
Lemma mem_map_inv: forall A B (f: A -> B) (l: list A) (x: B),
  mem x (LibList.map f l) -> exists y, mem y l /\ f y = x.
Proof.
  induction l; intros.
  * rew_list~ in *.
  * rew_list in *. tests : (x = f a). exists~ a.
    inv~ mem. pick mem (fun h => destruct (IHl x h) as [y ?]).
    exists y. split. apply mem_cons. right. all: intuition.
Qed.

(* index_of_Nth *)
Lemma index_of_Nth: forall A (H: Inhab A) (l: list A) (n: nat) a,
  Nth n l a -> l[n:int] = a.
Proof.
  introv Hnth. induction Hnth. rew_list~. replace (S n:int) with (n+1).
  rew_list~. lets: (Nth_inbound Hnth). math. math.
Qed.

(* Nth_of_index *)
Lemma Nth_of_index: forall A (H: Inhab A) (l: list A) (n: nat) a,
  index l (n:int) -> l[n:int] = a -> Nth n l a.
Proof.
  induction l; intros. rew_index. case_eq n. intros ->. rewrite read_zero in *.
  subst. constructor. intros m ->. constructor. apply IHl. rew_index.
  replace (S m:int) with (m+1) in *. rewrite read_succ in *. auto. rew_index.
  math.
Qed.

(** List enumerations **)

(* enumerate: annotate the list with its indexes starting from i *)
Fixpoint enumerate A (l: list A) (i: int): list (int * A) :=
  match l with
  | nil => nil
  | x :: tl => (i, x) :: enumerate tl (i + 1)
  end.

(* enumerate_map: a map function that has access to the indexes *)
Definition enumerate_map A B (f: int -> A -> B) (l: list A): list B :=
  LibList.map (fun '(i, x) => f i x) (enumerate l 0).

(* Then a few first lemmas for this structure... (I used LibList.map to make
   existing lemmas on maps available on enumerated maps) *)

(* read_enumerate: index-based description of the enumeration *)
Lemma read_enumerate: forall A (H: Inhab A) (l: list A) b i,
  index l i -> (enumerate l b)[i] = (b + i, l[i]).
Proof.
  induction l.
  * intros. inv @index. rew_list in *. math.
  * intros. simpl. rewrite read_cons_case. case_if~. subst. rewrite read_zero.
    replace (b+0)%I with b. auto. math.
    rewrite IHl. rewrite read_cons_case. case_if~.
    replace ((b+1)%I + (i-1)%I)%I with (b+i)%I. auto. math.
    inv @index. rew_list in *. rew_index.
Qed.

(* length_enumerate: length is preserved *)
Lemma length_enumerate: forall A (l: list A) b,
  length (enumerate l b) = length l.
Proof. induction l; intros; simpl. auto. rew_list. rewrites~ IHl. Qed.

(* read_enumerate_map: index-based description of the enumerated map *)
Lemma read_enumerate_map:
  forall A B (HA: Inhab A) (HB: Inhab B) (f: int -> A -> B) l i,
  index l i -> (enumerate_map f l)[i] = f i l[i].
Proof.
  introv Hind. unfold enumerate_map. rewrite read_map. rewrite read_enumerate.
  auto. auto. split. rew_index. rewrite <- length_eq. rewrite length_enumerate.
  rew_index.
Qed.

(* length_enumerate_map: length is preserved *)
Lemma length_enumerate_map: forall A B (f: int -> A -> B) l,
  length (enumerate_map f l) = length l.
Proof.
  intros. unfold enumerate_map. rewrite length_map. rewrites~ length_enumerate.
Qed.

Hint Rewrite read_enumerate : rew_list.
Hint Rewrite length_enumerate : rew_list.
Hint Rewrite read_enumerate_map : rew_list.
Hint Rewrite length_enumerate_map : rew_list.

(** Maps **)

(* TODO: Figure out why it won't compute unless [vm_compute] is used *)

Lemma map_update_diff: forall A B (M: LibMap.map A B) a' b' a b,
  a <> a' -> (binds M[a' := b'] a b) = (binds M a b).
Proof. intros. vm_compute. case_if~. Qed.

Lemma map_update_outside: forall A B (M: LibMap.map A B) a b a' b',
  a <> a' -> (binds M a b = binds M[a' := b'] a b).
Proof. intros. vm_compute. case_if~. Qed.

(* Monoid with union operation: to generate map objects using fold *)

Definition monoid_map (A B: Type) := monoid_make (@LibMap.union_impl A B) \{}.

Instance monoid_map_inst: forall A B, Monoid (monoid_map A B).
Proof. unfold monoid_map. constructor; unfold LibMap.union_impl.
  { unfold assoc.     intros x y z. extensionality k. destruct~ (z k). }
  { unfold neutral_l. intros x.     extensionality k. destruct~ (x k). }
  { unfold neutral_r. intros x.     extensionality k. destruct~ (x k). }
Qed.

(** Sets **)

(* (just for convenience really) *)
Notation "e ∈ X"   := (e \in X) (at level 30).
Notation "e ∉ X"   := (e \notin X) (at level 30).
Notation "X ⊆ Y"   := (X \c Y) (at level 29).
Notation "M \++ y" := (M \u \{ y }) (at level 31).
Notation "\set{ x ∈ E | P }" := (@set_st _ (fun x => x \in E /\ P))
 (at level 0, x ident, P at level 200) : set_scope.

Tactic Notation "rew_set" "~" :=
  rew_set; auto_tilde.

Hint Rewrite @card_empty : rew_set.
Hint Rewrite @card_diff_single : rew_set.

Hint Extern 1 (_ ∈ (_ \-- _)) => rew_set.
Hint Extern 1 (_ ∈ (_ \++ _)) => rew_set.
Hint Extern 1 (_ ∉ _) => unfold notin.
Hint Extern 1 (\{} ⊆ _) => apply empty_incl.
Hint Extern 2 (_ = \{}) => apply incl_empty_inv.

(* set_union_single_diff *)
Lemma set_union_single_diff: forall A (S: set A) (x y: A),
  x ∈ (S \++ y) -> x <> y -> x ∈ S.
Proof. intros. rew_set in *. intuition. Qed.

(* empty_of_card_zero: only {} has cardinal 0.
   I really feel that this one is missing. It's probably possible to get rid of
   the [finite S] hypothesis; I did not find a method for this. *)
Lemma empty_of_card_zero : forall A (S: set A),
  LibSet.finite S -> card S = 0%nat -> S = \{}.
Proof.
  introv H I. rewrites~ card_eq_length_to_list in I.
  apply LibList.length_zero_inv in I. apply is_empty_prove.
  intros. lets J : list_repr_to_list_of_finite S H. rewrite I in J.
  destruct J as [_ K]. destruct (K x) as [L M].
  pick @is_in (fun h => lets N: (M h)). inversion N.
Qed.

(* set_incl_remove: inclusion modulo removal of the same subset *)
Lemma set_incl_remove: forall A (X Y Z: set A), X ⊆ Y -> (X \- Z) ⊆ (Y \- Z).
Proof.
  intros. rew_set. intros. rewrite in_remove_eq in *. intuition.
  eapply incl_inv. all: eauto.
Qed.

(* incl_remove_eq: extension of in_remove_eq *)
Lemma incl_remove_eq: forall A (X Y Z:set A), X ⊆ (Y \- Z) = (X ⊆ Y /\ X \# Z).
Proof.
  intros. rewrite eq_prop_eq_iff. split.
  * rew_set. intros H. intuition auto. all: apply~ (H x).
  * rew_set. intros. rew_set. intuition eauto.
Qed.

(* double_incl_eq *)
Lemma double_incl_eq: forall A (X Y: set A), X ⊆ Y -> Y ⊆ X -> X = Y.
Proof. intros. rew_set in *. intuition. Qed.

(* set_in_out *)
Lemma set_in_out: forall A (S: set A) x y, x ∈ S -> x ∈ ((S \-- y) \++ y).
Proof. intros. rew_set. tests~ : (x = y). Qed.

(* Set tactics: these were useful to me when working on Dancing Links; they
   probably need a bit of refining and renaming. *)

(* weakset: turn k ∈ (D \-- y) into k ∈ D *)
Tactic Notation "weakset" "in" hyp(H) :=
  rew_set in H;
  destruct H as [H _].

(* strongset: turn k ∈ (D \++ y) into k ∈ D if k <> y is in the hypotheses *)
Tactic Notation "strongset" "in" hyp(Dk) :=
  match type of Dk with
  | ?k ∈ (?D \++ ?y) => repeat match goal with
    | h : k <> y |- _ => let x := fresh Dk in (
      rename Dk into x;
      assert (k ∈ D) as Dk by (apply (set_union_single_diff x h));
      clear x)
    | h : y <> k |- _ => apply neq_sym in h
    (* These ones appear when using intuition *)
    | h : k = y -> False |- _ => rewrite fold_neq in h
    | h : y = k -> False |- _ => rewrite fold_neq in h
    end
  end.

Tactic Notation "strongset" "in" "*" :=
  repeat match goal with
  | h: _ ∈ (_ \++ _) |- _ => strongset in h
  end.

(* Other lemmas using the above tactics *)

(* set_add_one_remove_same: analogous to set_remove_one_add_same *)
Lemma set_add_one_remove_same: forall A (X: set A) (x: A),
  x ∈ X -> X = (X \-- x) \++ x.
Proof.
  intros. rew_set. intros y. tests : (y = x). all: intuition auto.
  strongset in *. rew_set in *. intuition.
Qed.

(* set_union_single_incl: prove (X \++ x) ⊆ Y without introducing elements *)
Lemma set_union_single_incl: forall A (X Y: set A) (x: A),
  (X \++ x) ⊆ Y = (X ⊆ Y /\ x ∈ Y).
Proof.
  intros. rew_set. apply prop_ext. split; intros H.
  * auto.
  * intros y. intuition auto. tests~: (y = x). strongset in *. auto.
Qed.

(** Functional update **)

(* Make notation f[x := y] available for functions through fupdate *)
Instance update_inst : forall A B, BagUpdate A B (A -> B).
Proof. constructor. apply (@fupdate A B). Defined.
