(* A mutable permutation of [0, n). *)

type permutation = {
  next: int array;
  prev: int array;
}

(* The permutation and its inverse can be read in constant time. *)

val next: permutation -> int -> int
val prev: permutation -> int -> int

(* --- *)

val hide : permutation -> int -> unit
val show : permutation -> int -> unit

val without : permutation -> int -> (unit -> 'a) -> 'a
