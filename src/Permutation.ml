(* A permutation of [0, n) is stored in two arrays of size [n].
   The array [next] contains the permutation; the array [prev]
   contains its inverse. *)

type permutation = {
  next: int array;
  prev: int array;
}

(* The permutation and its inverse can be read in constant time. *)

let next p i =
  p.next.(i)

let prev p i =
  p.prev.(i)

(* --- *)

let hide p y =
  let w = prev p y
  and z = next p y in
  p.next.(w) <- z;
  p.prev.(z) <- w

let show p y =
  let w = prev p y
  and z = next p y in
  p.next.(w) <- y;
  p.prev.(z) <- y
;;

let without p y f =
  hide p y;
  let x = f () in
  show p y;
  x
