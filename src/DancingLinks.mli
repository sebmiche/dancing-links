(* An implementation of Knuth's Dancing Links algorithm. *)

type cover_problem

(* make_cover_problem - Create a cover_problem instance out of a matrix

   [make_cover_problem head matrix] creates a permutation-based representation
   of a generalized matrix cover problem.

   [matrix] is a matrix of booleans where each column represents an element of
   the set and each row represents a subset. A solution to the problem is a
   subset of the rows that intersect each primary column exactly once and each
   secondary column at most once. [head] specifies which columns are primary.

   Returns a cover_problem object that can be used by the [dance] function. *)
val make_cover_problem : bool array -> bool array array -> cover_problem

(* dlx - Solve the generalized exact cover problem using dancing links
   [dlx cp] calculates all the solutions to the generalized cover problem.
   Returns the number of the solutions of [cp]. *)
val dlx : cover_problem -> int

val get_nodes : unit -> int
