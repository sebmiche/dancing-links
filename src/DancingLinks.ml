(*
**	Knuth's Dancing Links algorithm for the generalized exact cover problem.
*)

open Permutation

type cover_problem = {
	(* This is a permutation of m + 1 elements when there are m columns;
	   element 0 is used as the root h.
	   Traversing [head] starting from the root h yields the list of active
	   columns of the problem. *)
	head: permutation;

	(* This is an array of m permutations of n + 1 elements when there are m
	   columns and n rows; elements with number 0 are used as column headers.
	   [cols] views the matrix in terms of *available choices* for each column
	   to be covered, and changes a lot during backtracking. *)
	cols: permutation array;

	(* This is an array of n permutations of m + 1 elements when there are n
	   rows and m columns; elements with number 0 are never used.
	   [rows] views the matrix in terms of *subset shapes* or, to put it
	   simply, the subsets provided by the client. Thus it never changes. *)
	rows: permutation array;

	(* Number of elements in each column. Used to select, at each step, the
	   column covered by the least amount of subsets to minimize branching.
	   This member is kept up-to-date with all operations on [cols]. *)
	size: int array;
}

(* A special version of [without] specifically for [cp.head] *)
let without_head cp c f =
  hide cp.head c;
  let x = f cp in
  show cp.head c;
  x

(* Semantic name for the problem root and column headers *)
let h = 0


(*
**	Operations on problem instances
*)

(* choose_column [cover_problem -> int]
   Chooses a column to cover for the next step of the algorithm. Returns h (0)
   if the given problem is already solved. *)

let rec improve cp (min_score, min_c) c =
	if c = h then min_c else

	(* Returns the column number c that minimizes the score in cp.size *)
	let score = cp.size.(c - 1) in
	let min = if score < min_score then (score, c) else (min_score, min_c) in
	improve cp min (next cp.head c)

let choose_column cp =
	improve cp (Array.length cp.rows + 1, h) (next cp.head h)


(* cover_column [cover_problem -> int -> (cover_problem -> 'a) -> 'a]
   This function temporarily covers column [c] of [cp], calls a user-provided
   function [f] while the column is covered, then uncovers [c].

   Covering a column removes it from the [head] list and hides from [cols] all
   rows that intersect it. [size] is also updated accordingly.

   @cp  Cover problem
   @c   Column of [cp] to cover temporarily
   @f   Callback function; will be passed the reduced problem instance
   Returns the return value of [f]. *)

(* Remove all elements right of [c] in row [r], then removes all subsets under
   [r] that cover [c] *)
let rec cover_element f cp r c first_c =
	if r = 0 then f () else

	(cp.size.(c - 1) <- cp.size.(c - 1) - 1;

	(* After removing the current row element, if there are elements left, call
	   recursively on same row; otherwise move to next row *)
	let ret = without cp.cols.(c - 1) r (fun () ->
		let c' = next cp.rows.(r - 1) c in
		if c' <> first_c then cover_element f cp r c' first_c
		else cover_element f cp (next cp.cols.(first_c - 1) r) first_c first_c)
	in

	cp.size.(c - 1) <- cp.size.(c - 1) + 1;
	ret)

let cover_column cp c f =
	(* Isolate the column header then remove the subsets that cross column c *)
	without_head cp c (fun cp ->
		cover_element f cp (next cp.cols.(c - 1) 0) c c)

(* select_row [cover_problem -> int -> int -> (cover_problem -> 'a) -> 'a]
   This function is similar to [cover_column] and uses the same callback style.
   It temporarily covers all visible columns on row [r] from [cp] and calls [f]
   before uncovering the columns.

   The function must be provided with the ID of any column in the row because
   rows don't have headings to indicate the location of active columns.

   @cp       Cover problem
   @r        Row of [cp] to cover
   @first_c  The ID of any active column in the row [r]
   @c        Current column (used internally, when invoking pass [first_c])
   @f        Callback function; will be passed the reduced problem instance
   Returns the return value of [f]. *)

let rec select_row cp r first_c c f =
	(* Iterate on all columns of the row; stop when coming back to [first_c] *)
	cover_column cp c (fun () ->
		let c' = next cp.rows.(r - 1) c in
		if c' = first_c then f cp
		else select_row cp r first_c c' f)


(*
**	Counting the number of solutions
*)

let nodes = ref 0

(* try_candidate [cover_problem -> int -> int -> int -> int]
   Tries all rows available to cover column [c] in [cp], starting at [row] and
   going down until the column heading. Returns [total] + the number of
   solutions for all checked rows. *)
let rec try_candidate cp c row total =
	if row = 0 then total else

	(* Find out how many solutions for this particular [row] *)
	let count = select_row cp row c c dlx
	in try_candidate cp c (next cp.cols.(c - 1) row) (total + count)

(* dlx [cover_problem -> int]
   Calculates the number of solutions to the provided problem instance. *)
and dlx cp =
	incr nodes;

	(* If cp is empty, then we've found a solution; return 1 *)
	if next cp.head h = h then 1 else

	(* Otherwise choose a column c to cover; it must not be empty *)
	let c = choose_column cp in
	if next cp.cols.(c - 1) 0 = 0 then 0 else

	(* Count the number of results for all subsets that cover c *)
	try_candidate cp c (next cp.cols.(c - 1) 0) 0

let get_nodes () = !nodes


(*
**	Generation of cover_problem objects
*)

(* link - Generate a cycle permutation for a subset of an array
   This function fills the target array [target] of size [n] with the next-part
   of a cycle. Numbers [i] such that [get i = true] are included in the cycle;
   all others are isolated and point to themselves. [reverse] specifies whether
   the traversal order of the source array must be reversed and can be used to
   generate the prev-part of the permutation. *)
let link get target reverse n =
	let diff = if reverse then (-1) else (+1) in

	let rec find_first i =
		if i = n then failwith "array without any 'true'" else
		if get i then i else find_first (i + 1) in

	let first = find_first 0 in

	let rec link_one base i =
		if i = first then target.(base) <- i else
		if get i
		then (target.(base) <- i; link_one i ((i + diff + n) mod n))
		else (target.(i) <- i; link_one base ((i + diff + n) mod n)) in
	link_one first ((first + diff + n) mod n)

(* link_array - Make a permutation out of a boolean array
   The provided array (of size n) corresponds to elements 1..n of the returned
   permutation; element 0 is used as a heading. It is included in the cycle if
   get 0 = true and left out otherwise. *)
let link_array get size =
	let perm = {
		next = Array.make size (-1);
		prev = Array.make size (-1);
	} in

	link get perm.next false size;
	link get perm.prev true  size;
	perm

(* make_cover_problem - Create a cover_problem instance out of a matrix *)
let make_cover_problem head_bools data_bools =

	let count_cols = Array.length head_bools in
	let count_rows = Array.length data_bools in

	(*** Column headings ***)

	(* Size is count_cols + 1 because the problem root h is in the list *)
	let get_head i = (i = 0) || head_bools.(i - 1) in
	let head = link_array get_head (count_cols + 1) in

	(*** Rows and columns ***)

	let get_row r i = (i > 0) && data_bools.(r).(i - 1) in
	let get_col c i = (i = 0) || data_bools.(i - 1).(c) in

	let rows = Array.init count_rows (fun r ->
		link_array (get_row r) (count_cols + 1)) in
	let cols = Array.init count_cols (fun c ->
		link_array (get_col c) (count_rows + 1)) in

	(*** Size of each column ***)

	let rec column_size i total c =
		if i > count_rows then total else
		column_size (i + 1) (total + if get_col c i then 1 else 0) c in

	let size = Array.init count_cols (column_size 1 0) in

	(*** Cover problem instance ***)

	let cover_problem = {
		head	= head;
		rows	= rows;
		cols	= cols;
		size	= size;
	} in

	cover_problem
