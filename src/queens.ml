(* N queens problem using algorithm DLX *)

open DancingLinks

(* Column numbers (optimization of trivial diagonals was omitted):
   0      .. N - 1     Ranks             R(0) .. R(N - 1)
   N      .. 2N - 1    Files             F(0) .. F(N - 1)
   2N     .. 4N - 2    Diagonals         A(0) .. A(2N - 2)
   4N - 1 .. 6N - 3    Anti-diagonals    B(0) .. B(2N - 2) *)
let _R n i = i
let _F n j = n + j
let _A n k = 2 * n + k
let _B n k = 4 * n - 1 + k

(* Each queen occupies a rank, a file and two diagonals *)
let make_row n row =
	let i = row / n
	and j = row mod n in
	Array.init (6 * n - 2) (fun k ->
		(k = _R n i || k = _F n j || k = _A n (i+j) || k = _B n (n-1-i+j)))

let n_queens_problem n =
	let cols = 6 * n - 2 in
	let rows = n * n in

	(* Primary columns are the ranks and files *)
	let head = Array.init cols ((>) (_A n 0)) in
	(* Boolean matrix that we wish to cover *)
	let data = Array.init rows (make_row n) in

	let cp    = DancingLinks.make_cover_problem head data in
	let count = DancingLinks.dlx cp in

	let log = fun x -> Printf.fprintf stderr x in
	log "\x1b[33;1mNumber of solutions for n = %d: %d\x1b[0m\n" n count;
	log "\x1b[33;1mNumber of search nodes: %d\x1b[0m\n" (get_nodes ())


(*
**	Main program
*)

let print_usage () =
	Printf.printf "usage: %s <n>\n  n: number of queens\n" Sys.argv.(0)

let main =
	if Array.length Sys.argv < 2 then begin
		print_usage ();
		exit(1)
	end;

	let n = int_of_string Sys.argv.(1) in
	n_queens_problem n
