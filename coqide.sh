#! /usr/bin/env bash

# Set your opam switch name here
switch=mycfml2

opam switch $switch
eval $(opam env)
coqide -R $HOME/.opam/$switch/lib/coq/user-contrib/CFML CFML \
       -R cfml/proofs/_cf PROOFS \
       -R proof/lib "" \
       -async-proofs off &
