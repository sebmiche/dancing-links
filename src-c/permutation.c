#include <permutation.h>

/* hide() - Isolate an element but keep enough information to show it later */
void hide(perm_t p, int y)
{
	int x = p.prev[y];
	int z = p.next[y];

	p.next[x] = z;
	p.prev[z] = x;
}

/* show() - Show the last hidden element */
void show(perm_t p, int y)
{
	int x = p.prev[y];
	int z = p.next[y];

	p.next[x] = y;
	p.prev[z] = y;
}
