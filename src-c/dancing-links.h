/*
**  Knuth's Dancing Links DLX algorithm
*/

#ifndef DANCING_LINKS_H
#define DANCING_LINKS_H

/* Opaque type */
struct cover_problem;
typedef struct cover_problem cover_problem_t;

/* dlx() - Solve the generalized exact cover problem using Dancing Links
   @cp  Fresh or partially-solved cover problem
   Returns the number of solutions of the instance. */
int dlx(cover_problem_t *cp);

/* make_cover() - Create a cover_problem_t object out of a matrix
   This function buils an internal, permutation-based representation of an
   exact cover instance provided as a matrix.

   @primary  Indicates which columns are primary
   @matrix   Matrix of subsets (rows) of the original set (columns)
   Returns a dynamically-allocated cover_problem_t object. */
cover_problem_t *make_cover(const int *primary,const int **matrix,int m,int n);

/* free_cover() - Free a cover problem instance and its associated memory
   @cp  Cover problem allocated by make_cover() */
void free_cover(cover_problem_t *cp);

#endif /* DANCING_LINKS_H */
