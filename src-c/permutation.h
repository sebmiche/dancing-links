/*
**  Permutations of { 0 .. n-1 }.
*/

#ifndef PERMUTATION_H
#define PERMUTATION_H

typedef struct
{
	/* No size; algorithm DLX does not need it. */
	int *next;
	int *prev;
} perm_t;

/* hide() - Isolate an element but keep enough information to show it later */
void hide(perm_t permutation, int element);

/* show() - Show the last hidden element */
void show(perm_t permutation, int element);

/* Iterate on next, but skip the starting element */
#define for_next(var, perm, start) \
	for(int var = perm.next[start]; var != start; var = perm.next[var])

/* Iterate on prev, but skip the starting element */
#define for_prev(var, perm, start) \
	for(int var = perm.prev[start]; var != start; var = perm.prev[var])

#endif /* PERMUTATION_H */
