#include <stdio.h>
#include <stdlib.h>
#include <dancing-links.h>

/* Column numbers:
   0      .. N - 1     Ranks             R(0) .. R(N - 1)
   N      .. 2N - 1    Files             F(0) .. F(N - 1)
   2N     .. 4N - 2    Diagonals         A(0) .. A(2N - 2)
   4N - 1 .. 6N - 3    Anti-diagonals    B(0) .. B(2N - 2) */
#define R(n, i) (i)
#define F(n, j) (n + j)
#define A(n, k) (2 * n + k)
#define B(n, k) (4 * n - 1 + k)

void n_queens_problem(int n)
{
	int cols = 6 * n - 2;
	int rows = n * n;

	/* Generate the header */

	int *head = malloc(cols * sizeof *head);
	for(int c = 0; c < cols; c++) head[c] = (c < A(n, 0));

	/* Generate the matrix */

	int *data = calloc(cols * rows, sizeof *data);
	int **matrix = malloc(rows * sizeof *matrix);
	for(int r = 0; r < rows; r++) matrix[r] = data + r * cols;

	/* Fill in the matrix */

	for(int r = 0; r < rows; r++)
	{
		int i = r / n, j = r % n;
		matrix[r][R(n, i)] = 1;
		matrix[r][F(n, j)] = 1;
		matrix[r][A(n, i + j)] = 1;
		matrix[r][B(n, n - 1 - i + j)] = 1;
	}

	/* Generate and solve the cover problem */

	cover_problem_t *cp = make_cover(head, (const int **)matrix,cols,rows);

	extern int nodes;
	int N = dlx(cp);
	printf("\e[33;1mTotal number of solutions for n = %d: %d\e[0m\n", n,N);
	printf("\e[33;1mTotal number of nodes: %d\e[0m\n", nodes);

	free_cover(cp);
	free(matrix);
	free(data);
	free(head);
}

int main(int argc, char **argv)
{
	if(argc < 2)
	{
		printf("usage: %s <n>\n  n: number of queens\n", argv[0]);
		exit(1);
	}

	int n = atoi(argv[1]);
	n_queens_problem(n);
	return 0;
}
