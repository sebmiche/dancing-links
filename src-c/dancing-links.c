/* Knuth's Dancing Links DLX algorithm */

#include <stdio.h>
#include <stdlib.h>
#include <permutation.h>
#include <dancing-links.h>

typedef struct cover_problem
{
	/* Number of columns, number of rows */
	int m, n;

	/* Permutation of m + 1 columns; element 0 is the root */
	perm_t head;
	/* Array of m permutations of n + 1 elements */
	perm_t *cols;
	/* Array of n permutations of m + 1 elements */
	perm_t *rows;
	/* Number of element in each column */
	int *size;

} cover_problem_t;

/* choose_column() - Select a column for the next dancing step
   @cp  Partially-solved cover problem
   Returns an uncovered primary column with the least 1s. */
int choose_column(cover_problem_t *cp)
{
	int column	= 0;
	int score	= cp->n + 1;

	for_next(c, cp->head, 0)
	{
		int sc = cp->size[c - 1];
		if(sc < score) score = sc, column = c;
	}

	return column;
}

/*
**  Covering and uncovering
*/

void cover_element(cover_problem_t *cp, int r, int c)
{
	cp->size[c - 1]--;
	hide(cp->cols[c - 1], r);
}

void cover_subset(cover_problem_t *cp, int r, int c_start)
{
	for_next(c, cp->rows[r - 1], c_start) cover_element(cp, r, c);
}

void cover_column(cover_problem_t *cp, int c)
{
	hide(cp->head, c);
	for_next(r, cp->cols[c - 1], 0) cover_subset(cp, r, c);
}

/* --- */

void uncover_element(cover_problem_t *cp, int r, int c)
{
	show(cp->cols[c - 1], r);
	cp->size[c - 1]++;
}

void uncover_subset(cover_problem_t *cp, int r, int c_start)
{
	for_prev(c, cp->rows[r - 1], c_start) uncover_element(cp, r, c);
}

void uncover_column(cover_problem_t *cp, int c)
{
	for_prev(r, cp->cols[c - 1], 0) uncover_subset(cp, r, c);
	show(cp->head, c);
}

/* --- */

void cover_row(cover_problem_t *cp, int r, int c_start)
{
	for_next(c, cp->rows[r - 1], c_start) cover_column(cp, c);
}

void uncover_row(cover_problem_t *cp, int r, int c_start)
{
	for_prev(c, cp->rows[r - 1], c_start) uncover_column(cp, c);
}

/*
**  Counting solutions
*/

/* Number of explored nodes in the search tree, ie. calls to dlx() */
int nodes = 0;

/* dlx() - Solve the generalized exact cover problem using Dancing Links */
int dlx(cover_problem_t *cp)
{
	int solutions = 0;
	nodes++;

	/* If cp->head is empty, then the problem is solved; return 1 */
	if(cp->head.next[0] == 0) return 1;

	/* Otherwise choose a column c to cover; it must not be empty */
	int c = choose_column(cp);
	if(cp->cols[c - 1].next[0] == 0) return 0;

	/* Count independant solutions for all rows that cove c */
	cover_column(cp, c);
	for_next(r, cp->cols[c - 1], 0)
	{
		cover_row(cp, r, c);
		solutions += dlx(cp);
		uncover_row(cp, r, c);
	}
	uncover_column(cp, c);

	return solutions;
}



/*
**  Generation of cover_problem_t objects
*/

void link(int (*get)(int), int *target, int reverse, int n)
{
	int first = 0;
	while(first < n && !get(first)) first++;
	if(first >= n) puts("erray without any 'true'"), exit(1);

	int diff = (reverse) ? (-1) : (+1), base = first, i;

	for(i = (first + diff + n) % n; i != first; i = (i + diff + n) % n)
	{
		if(get(i)) target[base] = i, base = i;
		else target[i] = i;
	}
	target[base] = i;
}

perm_t link_array(int (*get)(int), int size)
{
	perm_t perm = {
		.next = malloc(size * sizeof *perm.next),
		.prev = malloc(size * sizeof *perm.prev),
	};

	link(get, perm.next, 0, size);
	link(get, perm.prev, 1, size);
	return perm;
}

/* make_cover() - Create a cover_problem_t object out of a matrix */
cover_problem_t *make_cover(const int *primary, const int **matrix,int m,int n)
{
	/* Proper failure that deallocates memory */
	#define fail() do {		\
		free_cover(cp);		\
		return NULL;		\
	} while(0)

	/* Automatically fail() if malloc() returns NULL */
	#define malloc(n) ({		\
		void *x = malloc(n);	\
		if(!x) fail();		\
		x;			\
	})

	cover_problem_t *cp = NULL;

	cp = malloc(sizeof *cp);
	cp->m = m, cp->n = n;

	/*** Column headings ***/

	int get_head(int i) { return (i == 0) || primary[i - 1]; }
	cp->head = link_array(get_head, m + 1);

	/*** Rows and columns ***/

	cp->rows = malloc(n * sizeof *cp->rows);

	for(int r = 0; r < n; r++)
	{
		int get_row(int i) { return (i > 0) && matrix[r][i - 1]; }
		cp->rows[r] = link_array(get_row, m + 1);
	}

	cp->cols = malloc(m * sizeof *cp->cols);

	for(int c = 0; c < m; c++)
	{
		int get_col(int i) { return (i == 0) || matrix[i - 1][c]; }
		cp->cols[c] = link_array(get_col, n + 1);
	}

	/*** Size of each column ***/

	cp->size = malloc(m * sizeof *cp->size);

	for(int c = 0; c < m; c++)
	{
		int count = 0;
		for(int r = 0; r < n; r++) count += !!matrix[r][c];
		cp->size[c] = count;
	}

	/*** Cover problem instance ***/

	return cp;
}

/* free_cover() - Free a cover problem instance and its associated memory */
void free_cover(cover_problem_t *cp)
{
	if(!cp) return;
	free(cp->head.next), free(cp->head.prev);

	for(int r = 0; r < cp->n; r++)
	{
		free(cp->rows[r].next), free(cp->rows[r].prev);
	}
	free(cp->rows);

	for(int c = 0; c < cp->m; c++)
	{
		free(cp->cols[c].next), free(cp->cols[c].prev);
	}
	free(cp->cols);

	free(cp->size);
	free(cp);
}
